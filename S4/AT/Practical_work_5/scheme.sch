EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:scheme-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L DD_NOR3_1 DD?
U 1 1 5AF71986
P 3600 1550
F 0 "DD?" H 3600 2050 70  0001 C CNN
F 1 "DD_NOR3_1" H 3600 1050 70  0001 C CNN
F 2 "" H 3600 1650 70  0001 C CNN
F 3 "" H 3600 1650 70  0001 C CNN
	1    3600 1550
	1    0    0    -1  
$EndComp
Entry Wire Line
	1350 1750 1450 1850
Entry Wire Line
	1350 1900 1450 2000
Entry Wire Line
	1350 2050 1450 2150
Text Label 1000 1750 0    60   ~ 0
J
Text Label 1000 1900 0    60   ~ 0
C
Text Label 1000 2050 0    60   ~ 0
K
$Comp
L DD_NOR3_1 DD?
U 1 1 5AF71A72
P 3600 2650
F 0 "DD?" H 3600 3150 70  0001 C CNN
F 1 "DD_NOR3_1" H 3600 2150 70  0001 C CNN
F 2 "" H 3600 2750 70  0001 C CNN
F 3 "" H 3600 2750 70  0001 C CNN
	1    3600 2650
	1    0    0    -1  
$EndComp
Text Label 3050 2650 0    60   ~ 0
R1
Text Label 3050 2850 0    60   ~ 0
R0
Text Label 3050 1550 0    60   ~ 0
S1
Text Label 3050 1350 0    60   ~ 0
S0
Text Label 4200 2650 0    60   ~ 0
Q1
Text Label 4150 1550 0    60   ~ 0
¬Q1
$Comp
L DD_NOR3_1 DD?
U 1 1 5AF71CC4
P 3600 4250
F 0 "DD?" H 3600 4750 70  0001 C CNN
F 1 "DD_NOR3_1" H 3600 3750 70  0001 C CNN
F 2 "" H 3600 4350 70  0001 C CNN
F 3 "" H 3600 4350 70  0001 C CNN
	1    3600 4250
	1    0    0    -1  
$EndComp
$Comp
L DD_NOR3_1 DD?
U 1 1 5AF71CCA
P 3600 5350
F 0 "DD?" H 3600 5850 70  0001 C CNN
F 1 "DD_NOR3_1" H 3600 4850 70  0001 C CNN
F 2 "" H 3600 5450 70  0001 C CNN
F 3 "" H 3600 5450 70  0001 C CNN
	1    3600 5350
	1    0    0    -1  
$EndComp
Text Label 3050 5350 0    60   ~ 0
R2
Text Label 3050 5550 0    60   ~ 0
R0
Text Label 3050 4250 0    60   ~ 0
S2
Text Label 3050 4050 0    60   ~ 0
S0
Text Label 4200 5350 0    60   ~ 0
Q2
Text Label 4150 4250 0    60   ~ 0
¬Q2
$Comp
L DD_NOR3_1 DD?
U 1 1 5AF71D0A
P 2100 1550
F 0 "DD?" H 2100 2050 70  0001 C CNN
F 1 "DD_NOR3_1" H 2100 1050 70  0001 C CNN
F 2 "" H 2100 1650 70  0001 C CNN
F 3 "" H 2100 1650 70  0001 C CNN
	1    2100 1550
	1    0    0    -1  
$EndComp
$Comp
L DD_NOR3_1 DD?
U 1 1 5AF71D29
P 2100 2650
F 0 "DD?" H 2100 3150 70  0001 C CNN
F 1 "DD_NOR3_1" H 2100 2150 70  0001 C CNN
F 2 "" H 2100 2750 70  0001 C CNN
F 3 "" H 2100 2750 70  0001 C CNN
	1    2100 2650
	1    0    0    -1  
$EndComp
$Comp
L DD_NOR3_1 DD?
U 1 1 5AF71D34
P 2100 4250
F 0 "DD?" H 2100 4750 70  0001 C CNN
F 1 "DD_NOR3_1" H 2100 3750 70  0001 C CNN
F 2 "" H 2100 4350 70  0001 C CNN
F 3 "" H 2100 4350 70  0001 C CNN
	1    2100 4250
	1    0    0    -1  
$EndComp
$Comp
L DD_NOR3_1 DD?
U 1 1 5AF71D3F
P 2100 5350
F 0 "DD?" H 2100 5850 70  0001 C CNN
F 1 "DD_NOR3_1" H 2100 4850 70  0001 C CNN
F 2 "" H 2100 5450 70  0001 C CNN
F 3 "" H 2100 5450 70  0001 C CNN
	1    2100 5350
	1    0    0    -1  
$EndComp
Entry Wire Line
	1450 3950 1550 4050
Entry Wire Line
	1450 4150 1550 4250
Entry Wire Line
	1450 2750 1550 2850
Entry Wire Line
	1450 2550 1550 2650
Entry Wire Line
	1450 2350 1550 2450
Entry Wire Line
	1450 1650 1550 1750
Entry Wire Line
	1450 1450 1550 1550
Entry Wire Line
	1450 1250 1550 1350
Entry Wire Line
	1450 3100 1550 3200
Entry Wire Line
	1450 3600 1550 3700
Entry Wire Line
	1450 900  1550 1000
Entry Wire Line
	1450 5800 1550 5900
Entry Wire Line
	1350 1600 1450 1700
Text Label 1000 1600 0    60   ~ 0
R0
Wire Wire Line
	1350 1750 1000 1750
Wire Wire Line
	1350 1900 1000 1900
Wire Wire Line
	1000 2050 1350 2050
Wire Wire Line
	3250 1350 3050 1350
Wire Wire Line
	3050 1350 3050 1000
Wire Wire Line
	3250 2850 3050 2850
Wire Wire Line
	3050 2850 3050 3200
Wire Wire Line
	3250 2650 2450 2650
Wire Wire Line
	3250 1550 2450 1550
Wire Wire Line
	3250 1750 3050 1750
Wire Wire Line
	3250 2450 3050 2450
Wire Wire Line
	3950 2650 4350 2650
Wire Wire Line
	3950 1550 4350 1550
Wire Wire Line
	3050 1750 3050 2000
Wire Wire Line
	3050 2450 3050 2200
Wire Wire Line
	4050 2200 4050 2650
Connection ~ 4050 2650
Wire Wire Line
	3050 2000 4050 2200
Wire Wire Line
	4050 2000 4050 1550
Connection ~ 4050 1550
Wire Wire Line
	3050 2200 4050 2000
Wire Wire Line
	3250 4050 3050 4050
Wire Wire Line
	3050 4050 3050 3700
Wire Wire Line
	3250 5550 3050 5550
Wire Wire Line
	3050 5550 3050 5900
Wire Wire Line
	3250 5350 2450 5350
Wire Wire Line
	3250 4250 2450 4250
Wire Wire Line
	3250 4450 3050 4450
Wire Wire Line
	3250 5150 3050 5150
Wire Wire Line
	3950 5350 4350 5350
Wire Wire Line
	3950 4250 4350 4250
Wire Wire Line
	3050 4450 3050 4700
Wire Wire Line
	3050 5150 3050 4900
Wire Wire Line
	4050 4900 4050 5350
Connection ~ 4050 5350
Wire Wire Line
	3050 4700 4050 4900
Wire Wire Line
	4050 4700 4050 4250
Connection ~ 4050 4250
Wire Wire Line
	3050 4900 4050 4700
Wire Wire Line
	1750 4050 1550 4050
Wire Wire Line
	1550 4250 1750 4250
Wire Wire Line
	1750 1350 1550 1350
Wire Wire Line
	1550 1550 1750 1550
Wire Wire Line
	1550 1750 1750 1750
Wire Wire Line
	1550 2450 1750 2450
Wire Wire Line
	1550 2650 1750 2650
Wire Wire Line
	1550 2850 1750 2850
Wire Wire Line
	3050 3200 1550 3200
Wire Wire Line
	3050 3700 1550 3700
Wire Wire Line
	3050 1000 1550 1000
Wire Wire Line
	3050 5900 1550 5900
Wire Wire Line
	1350 1600 1000 1600
Entry Wire Line
	1350 2200 1450 2300
Text Label 1000 2200 0    60   ~ 0
S0
Wire Wire Line
	1350 2200 1000 2200
Entry Wire Line
	1450 5050 1550 5150
Entry Wire Line
	1450 5250 1550 5350
Wire Wire Line
	1550 5150 1750 5150
Wire Wire Line
	1750 5350 1550 5350
Text Label 1550 3700 0    60   ~ 0
S0
Text Label 1550 5900 0    60   ~ 0
R0
Text Label 1550 1000 0    60   ~ 0
S0
Text Label 1550 3200 0    60   ~ 0
R0
Text Label 1550 1350 0    60   ~ 0
Q2
Text Label 1550 1550 0    60   ~ 0
¬J
Text Label 1550 1750 0    60   ~ 0
¬C
Text Label 1550 2450 0    60   ~ 0
¬Q2
Text Label 1550 2650 0    60   ~ 0
¬K
Text Label 1550 2850 0    60   ~ 0
¬C
Text Label 1550 4050 0    60   ~ 0
¬Q1
Text Label 1550 5150 0    60   ~ 0
Q1
Text Label 1550 4250 0    60   ~ 0
C
Text Label 1550 5350 0    60   ~ 0
C
$Comp
L 0 U?
U 1 1 5AF72CF1
P 1750 4450
F 0 "U?" H 1750 4550 60  0001 C CNN
F 1 "0" H 1750 4350 60  0001 C CNN
F 2 "" H 1750 4450 60  0000 C CNN
F 3 "" H 1750 4450 60  0000 C CNN
	1    1750 4450
	1    0    0    -1  
$EndComp
$Comp
L 0 U?
U 1 1 5AF72D05
P 1750 5550
F 0 "U?" H 1750 5650 60  0001 C CNN
F 1 "0" H 1750 5450 60  0001 C CNN
F 2 "" H 1750 5550 60  0000 C CNN
F 3 "" H 1750 5550 60  0000 C CNN
	1    1750 5550
	1    0    0    -1  
$EndComp
$Comp
L DD_NOR3_1 DD?
U 1 1 5AF72D45
P 2100 6500
F 0 "DD?" H 2100 7000 70  0001 C CNN
F 1 "DD_NOR3_1" H 2100 6000 70  0001 C CNN
F 2 "" H 2100 6600 70  0001 C CNN
F 3 "" H 2100 6600 70  0001 C CNN
	1    2100 6500
	1    0    0    -1  
$EndComp
$Comp
L DD_NOR3_1 DD?
U 1 1 5AF72D5A
P 3600 6500
F 0 "DD?" H 3600 7000 70  0001 C CNN
F 1 "DD_NOR3_1" H 3600 6000 70  0001 C CNN
F 2 "" H 3600 6600 70  0001 C CNN
F 3 "" H 3600 6600 70  0001 C CNN
	1    3600 6500
	1    0    0    -1  
$EndComp
$Comp
L DD_NOR3_1 DD?
U 1 1 5AF72D65
P 5100 6500
F 0 "DD?" H 5100 7000 70  0001 C CNN
F 1 "DD_NOR3_1" H 5100 6000 70  0001 C CNN
F 2 "" H 5100 6600 70  0001 C CNN
F 3 "" H 5100 6600 70  0001 C CNN
	1    5100 6500
	1    0    0    -1  
$EndComp
Wire Bus Line
	1750 7050 5700 7050
Entry Wire Line
	4350 4250 4450 4350
Entry Wire Line
	4350 5350 4450 5450
Entry Wire Line
	1450 6200 1550 6300
Entry Wire Line
	2450 6950 2550 7050
Entry Wire Line
	3950 6950 4050 7050
Entry Wire Line
	5450 6950 5550 7050
Wire Wire Line
	1550 6300 1750 6300
Wire Wire Line
	2450 6500 2450 6950
Wire Wire Line
	3950 6500 3950 6950
Wire Wire Line
	5450 6500 5450 6950
Wire Wire Line
	2950 6300 3250 6300
Wire Wire Line
	2950 6300 2950 6950
Entry Wire Line
	2950 6950 3050 7050
Entry Wire Line
	4450 6950 4550 7050
Wire Wire Line
	4450 6950 4450 6300
Wire Wire Line
	4450 6300 4750 6300
$Comp
L 0 U?
U 1 1 5AF72FC6
P 1750 6500
F 0 "U?" H 1750 6600 60  0001 C CNN
F 1 "0" H 1750 6400 60  0001 C CNN
F 2 "" H 1750 6500 60  0000 C CNN
F 3 "" H 1750 6500 60  0000 C CNN
	1    1750 6500
	1    0    0    -1  
$EndComp
$Comp
L 0 U?
U 1 1 5AF72FD1
P 1750 6700
F 0 "U?" H 1750 6800 60  0001 C CNN
F 1 "0" H 1750 6600 60  0001 C CNN
F 2 "" H 1750 6700 60  0000 C CNN
F 3 "" H 1750 6700 60  0000 C CNN
	1    1750 6700
	1    0    0    -1  
$EndComp
$Comp
L 0 U?
U 1 1 5AF72FDC
P 3250 6500
F 0 "U?" H 3250 6600 60  0001 C CNN
F 1 "0" H 3250 6400 60  0001 C CNN
F 2 "" H 3250 6500 60  0000 C CNN
F 3 "" H 3250 6500 60  0000 C CNN
	1    3250 6500
	1    0    0    -1  
$EndComp
$Comp
L 0 U?
U 1 1 5AF72FE7
P 3250 6700
F 0 "U?" H 3250 6800 60  0001 C CNN
F 1 "0" H 3250 6600 60  0001 C CNN
F 2 "" H 3250 6700 60  0000 C CNN
F 3 "" H 3250 6700 60  0000 C CNN
	1    3250 6700
	1    0    0    -1  
$EndComp
$Comp
L 0 U?
U 1 1 5AF72FF2
P 4750 6500
F 0 "U?" H 4750 6600 60  0001 C CNN
F 1 "0" H 4750 6400 60  0001 C CNN
F 2 "" H 4750 6500 60  0000 C CNN
F 3 "" H 4750 6500 60  0000 C CNN
	1    4750 6500
	1    0    0    -1  
$EndComp
$Comp
L 0 U?
U 1 1 5AF72FFD
P 4750 6700
F 0 "U?" H 4750 6800 60  0001 C CNN
F 1 "0" H 4750 6600 60  0001 C CNN
F 2 "" H 4750 6700 60  0000 C CNN
F 3 "" H 4750 6700 60  0000 C CNN
	1    4750 6700
	1    0    0    -1  
$EndComp
Text Label 1550 6300 0    60   ~ 0
C
Text Label 4450 6950 0    60   ~ 0
K
Text Label 2950 6950 0    60   ~ 0
J
Text Label 2450 6950 0    60   ~ 0
¬C
Text Label 5450 6950 0    60   ~ 0
¬K
Text Label 3950 6950 0    60   ~ 0
¬J
Wire Bus Line
	1450 7050 1800 7050
Wire Bus Line
	5700 7050 5700 5800
Wire Bus Line
	5700 5800 4450 5800
Wire Bus Line
	4450 5800 4450 850 
Entry Wire Line
	4350 2650 4450 2750
Entry Wire Line
	4350 1550 4450 1650
Wire Bus Line
	1450 850  1450 7050
Entry Wire Line
	4450 3200 4550 3300
Wire Wire Line
	4550 3300 5050 3300
Text Label 4600 3300 0    60   ~ 0
Q2
Entry Wire Line
	4450 3550 4550 3650
Wire Wire Line
	4550 3650 5050 3650
Text Label 4550 3650 0    60   ~ 0
¬Q2
$EndSCHEMATC
