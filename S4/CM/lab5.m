#! /usr/bin/octave
clear; clc;
# F(x) = 600/(x^2-6x+36)
# F'(x) = (1200*(3 - x))/(36 - 6 x + x^2)^2
# [a, b] = [0; 2]

NOISE = 1*exp(-20);
BEGIN_N = 10;
END_N = 100;
LEFT_BORDER = 0;
RIGHT_BORDER = 2;
SOME_POINT = 1;
# Dots for plot
PLOT_DOTS_X = LEFT_BORDER-1:0.01:RIGHT_BORDER+1;

alphas_dif_m1 = [];
alphas_dif_m2 = [];
alphas_dif_m3 = [];
alphas_dif_m4 = [];

alphas_dif_m5 = [];
alphas_dif_m6 = [];

alphas_int_m1 = [];
alphas_int_m2 = [];
alphas_int_m3 = [];
alphas_int_m4 = [];

for n = BEGIN_N:END_N
  h = (RIGHT_BORDER - LEFT_BORDER) / (n-1);
  dots_x = LEFT_BORDER:h:RIGHT_BORDER;
  dots_y = arrayfun(@(x) 600/(x^2-6*x+36) + NOISE*(0.5-rand(1)), dots_x);

  if (n == BEGIN_N + 2)
  # Plot for function
  figure
    grid on
    grid minor on
    hold on
    plot(dots_x, dots_y, "*", "linewidth", 3, "color", "blue");
    plot(PLOT_DOTS_X, arrayfun(@(x) 600/(x^2-6*x+36), PLOT_DOTS_X), "-", "linewidth", 2, "color", "red");
    title(["Lab 5: Differentiation and Intergation.\n Function and dots for n = ", int2str(n)]);
  endif

  #
  #      DIFFERENTIATION
  #
  # 1. f'(x0) = (f(x1) - f(x0))/(x1 - x0) + O(h)
  # 2. f'(x0) = (f(x1) - f(xm1))/(2h) + O(h^2)
  # 3. f'(x0) = (-3f(x0) + 4f(x1) - f(x2))/(2h) + O(h^2)
  # 4. f'(x0) = (-11f(x0) + 18f(x1) - 9f(x2) + 2f(x3))/(6h) + O(h^3)
  # 5. f"(x0) = (f(x1) - 2f(x0) + f(xm1))/(2h^2) + O(h^2)
  # 6. f"(x0) = (2f(x0) - 5f(x1) + 4f(x2) - f(x3))/(h^2) + O(h^2)
  # 
  #
    
  # Find closest point to differentiate
  closest_one = [];
  closest_point_index = dsearchn(dots_x', SOME_POINT);
  x0 = closest_point_index;
  xm1 = closest_point_index - 1;
  x1 = closest_point_index + 1;
  x2 = closest_point_index + 2;
  x3 = closest_point_index + 3;

  # F'(x) = (1200*(3 - x))/(36 - 6*x + x^2)^2
  true_derivative = arrayfun(@(x) ((1200*(3 - x))/((36 - 6*x + x^2)^2)), dots_x(x0));
  method1_derivative = (dots_y(x1) - dots_y(x0))/h;
  method2_derivative = (dots_y(x1) - dots_y(xm1))/(2*h);
  method3_derivative = (-3*dots_y(x0) + 4*dots_y(x1) - dots_y(x2))/(2*h);
  method4_derivative = (-11*dots_y(x0) + 18*dots_y(x1) -9*dots_y(x2) + 2*dots_y(x3))/(6*h);

  # F"(x) = (3600*(-6 + x)*x)/(36 - 6*x + x^2)^3
  true_second_order_derivative = arrayfun(@(x) ((3600*(-6 + x)*x)/(36 - 6*x + x^2)^3), dots_x(x0));
  method5_so_derivative = (dots_y(x1) - 2*dots_y(x0) + dots_y(xm1))/(h^2);
  method6_so_derivative = (2*dots_y(x0) - 5*dots_y(x1) + 4*dots_y(x2) - dots_y(x3))/(h^2);

  method1_delta = abs(true_derivative - method1_derivative);
  method2_delta = abs(true_derivative - method2_derivative);
  method3_delta = abs(true_derivative - method3_derivative);
  method4_delta = abs(true_derivative - method4_derivative);
  method5_delta = abs(true_second_order_derivative - method5_so_derivative);
  method6_delta = abs(true_second_order_derivative - method6_so_derivative);

  method1_alpha = log(method1_delta)/log(h);
  method2_alpha = log(method2_delta)/log(h);
  method3_alpha = log(method3_delta)/log(h);
  method4_alpha = log(method4_delta)/log(h);
  method5_alpha = log(method5_delta)/log(h);
  method6_alpha = log(method6_delta)/log(h);

  alphas_dif_m1 = [alphas_dif_m1; method1_alpha];
  alphas_dif_m2 = [alphas_dif_m2; method2_alpha];
  alphas_dif_m3 = [alphas_dif_m3; method3_alpha];
  alphas_dif_m4 = [alphas_dif_m4; method4_alpha];
  alphas_dif_m5 = [alphas_dif_m5; method5_alpha];
  alphas_dif_m6 = [alphas_dif_m6; method6_alpha];
  
  #
  #      INTEGRATION
  #
  # 1. Left rectangles
  # 2. Middle rectangles
  # 3. Trapeze
  # 4. Simpson
  # 
  #

  # Indef int of F(x) = (200*arctan((-3 + x)/(3*sqrt(3))))/sqrt(3)
  left_eval = arrayfun(@(x) (200*atan((-3 + x)/(3*sqrt(3))))/sqrt(3), LEFT_BORDER);
  right_eval = arrayfun(@(x) (200*atan((-3 + x)/(3*sqrt(3))))/sqrt(3), RIGHT_BORDER);
  true_int =  right_eval - left_eval;

  method1_int = h*sum(dots_y(1:end-1));
  method2_int = h*sum((dots_y(1:end-1) + dots_y(2:end))/2);
  method3_int = h*sum((dots_y(1:end-1) + dots_y(2:end))/2); # exact same thing ???
  method4_int = (h/3)*sum(dots_y(1:2:end-2) + 4*dots_y(2:2:end-1) + dots_y(3:2:end));
  if (mod(n, 2) == 0) # Last left
    method4_int += h*((dots_y(end-1) + dots_y(end))/2);
  endif

  method1_int_delta = abs(true_int - method1_int);
  method2_int_delta = abs(true_int - method2_int);
  method3_int_delta = abs(true_int - method3_int);
  method4_int_delta = abs(true_int - method4_int);

  method1_int_alpha = log(method1_int_delta)/log(h);
  method2_int_alpha = log(method2_int_delta)/log(h);
  method3_int_alpha = log(method3_int_delta)/log(h);
  method4_int_alpha = log(method4_int_delta)/log(h);
  
  alphas_int_m1 = [alphas_int_m1; method1_int_alpha];
  alphas_int_m2 = [alphas_int_m2; method2_int_alpha];
  alphas_int_m3 = [alphas_int_m3; method3_int_alpha];
  alphas_int_m4 = [alphas_int_m4; method4_int_alpha];

endfor

figure
  grid on
  grid minor on
  hold on
  plot(BEGIN_N:END_N, alphas_dif_m1, "-", "linewidth", 2, "color", "red");
  plot(BEGIN_N:END_N, alphas_dif_m2, "-", "linewidth", 2, "color", "blue");
  plot(BEGIN_N:END_N, alphas_dif_m3, "-", "linewidth", 2, "color", "green");
  plot(BEGIN_N:END_N, alphas_dif_m4, "-", "linewidth", 2, "color", "magenta");
  legend("1. Simple", "2. Double", "3. 3-dotted", "4. 4-dotted");
  title("Lab 5: Alphas for single differentiation.");

figure
  grid on
  grid minor on
  hold on
  plot(BEGIN_N:END_N, alphas_dif_m5, "-", "linewidth", 2, "color", "green");
  plot(BEGIN_N:END_N, alphas_dif_m6, "-", "linewidth", 2, "color", "magenta");
  legend("5. 3-dotted", "6. 4-dotted");
  title("Lab 5: Alphas for double differentiation.");
  
figure
  grid on
  grid minor on
  hold on
  plot(BEGIN_N:END_N, alphas_int_m1, "-", "linewidth", 2, "color", "red");
  plot(BEGIN_N:END_N, alphas_int_m2, "-", "linewidth", 2, "color", "blue");
  plot(BEGIN_N:END_N, alphas_int_m3, "--", "linewidth", 2, "color", "green");
  plot(BEGIN_N+1:2:END_N, alphas_int_m4(2:2:end), "-", "linewidth", 2, "color", "magenta");
  legend("1. Left rect.", "2. Center rect.", "3. Trapeze", "4. Simpson");
  title("Lab 5: Alphas for integration.");
  
  