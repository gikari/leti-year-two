#!/usr/bin/octave
clear; clc;
PERSITION = 0.01;
SIZE = 1; 
do
  SIZE = SIZE + 1;
  A = ones(SIZE, 1);
  SOME_X_MATRIX = round(rand(SIZE,1)*100);
  #disp(SOME_X_MATRIX);
  #disp(A);
  for x = 1:1:SIZE-1
    #disp(A);
    A = [A, (ones(SIZE, 1).*SOME_X_MATRIX).^x];
  endfor
  #disp(A);
  #disp(A);
  INV_A = inv(A);
  PRODUCT = A * INV_A;
  DIFFERENCE = PRODUCT - eye(SIZE);
  MAX_ELEM = max(max(DIFFERENCE));
  ACCURACY = abs(MAX_ELEM);
  printf('Calculated %dx%d matrix with ACCURACY = %e\n', SIZE, SIZE, ACCURACY)
until (ACCURACY >= 0.01);
printf('Max matrix size is %d\n', SIZE);

