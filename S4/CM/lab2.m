#! /usr/bin/octave
#FUNC = 2*cos(2*x + 1)
clear; clc;
#X = [2; 4; 6; 8; 10;];
X = (2:2:10)';
#F_X = [0.5673; -1.8223; 1.8149; -0.5503; -1.0955;];
F_X = arrayfun(@(x) 2*cos(2*x + 1), X);

#Cheb. nodes
C_X = affine_cheb_nodes(2, 10, 5);
C_F_X = arrayfun(@(x) 2*cos(2*x + 1), C_X);

POINTS_FOR_PLOT = 1.9:0.1:10.1;

# First plot
figure
  grid on
  grid minor on
  hold on
  title("Lab 2: Interpolation");
  # FUNC
  plot(POINTS_FOR_PLOT, 2*cos(2*POINTS_FOR_PLOT + 1));
  # LAGRANGE
  plot(POINTS_FOR_PLOT, lagrange_pol(POINTS_FOR_PLOT, X, F_X), '*','linewidth', 2, 'color', 'red');
  # NEWTON
  plot(POINTS_FOR_PLOT, newton_pol(POINTS_FOR_PLOT, X, F_X), 'color', 'black', 'linewidth', 2);
  # CHEBYSHEV LAGRANGE
  plot(POINTS_FOR_PLOT, lagrange_pol(POINTS_FOR_PLOT, C_X, C_F_X),'linewidth', 3, 'color', 'green');
  
  legend("Function", "Lagrange", "Newton", 'Lag. Cheb. nodes');

figure
  grid on
  grid minor on
  hold on
  title("Lab 2: Chebyshev nodes");

# Lagrange for range [5; 9] dots with deltas
for i = 5:9
  C_X = affine_cheb_nodes(2, 10, i);
  C_F_X = arrayfun(@(x) 2*cos(2*x + 1), C_X);
  plot(POINTS_FOR_PLOT, lagrange_pol(POINTS_FOR_PLOT, C_X, C_F_X),'--','linewidth', 1);
endfor

# For 10 dots (marked)
C_X = affine_cheb_nodes(2, 10, 10);
C_F_X = arrayfun(@(x) 2*cos(2*x + 1), C_X);
plot(POINTS_FOR_PLOT, lagrange_pol(POINTS_FOR_PLOT, C_X, C_F_X),'linewidth', 2, 'color', 'red');
# Fucnction
plot(POINTS_FOR_PLOT, 2*cos(2*POINTS_FOR_PLOT + 1), 'color', 'blue', 'linewidth', 2);
# Vert lines
plot([C_X,C_X],[-4,4], '-.', 'color', 'black'); 

legend('5','6','7','8','9', '10', 'func');

# Differencies for [5; 30]
random_dot = 4.6;
range = 1:40;
DELTAS = [];
ALPHAS = [];
for i = range
  C_X = affine_cheb_nodes(2, 10, i);
  C_F_X = arrayfun(@(x) 2*cos(2*x + 1), C_X);
  delta = abs(lagrange_pol(random_dot, C_X, C_F_X) - 2*cos(2*random_dot + 1));
  alpha = -log(delta)/log(i);
  DELTAS = [DELTAS; delta];
  ALPHAS = [ALPHAS; alpha];
endfor

figure;
  grid on;
  grid minor on;
  hold on;
  xlabel('n');
  ylabel('Delta');
  title("Lab 2: Deltas");
plot(range, DELTAS, 'color', 'blue', 'linewidth', 2);

figure;
  grid on;
  grid minor on;
  hold on;
  xlabel('n');
  ylabel('Alpha');
  title("Lab 2: Alphas");
plot(range, ALPHAS, 'color', 'red', 'linewidth', 2);
