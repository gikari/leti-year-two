#! /usr/bin/octave
clear; clc;
# F(x) = 600/(x^2-6x+36)
# [a, b] = [-3; 4]
PERSITION = 0.001;

for POL_DEGREE = [5]
  if POL_DEGREE == 10
    PERSITION *= 0.1;
  endif
  nodes_x = affine_cheb_nodes(-3, 4, POL_DEGREE + 2);

  epsilon = 10;
  new_pol = [];
  while epsilon > PERSITION
    f_x = arrayfun(@(x) 600/(x^2-6*x+36), nodes_x);

    lin_eq_sys_A = [];
    lin_eq_sys_B = [];
    # Creating system A*x = B
    for i = 1:(POL_DEGREE+2)
      sum_of_alpha = [];
      for k = 1:(POL_DEGREE+1)
        sum_of_alpha = [nodes_x(i)^(k-1), sum_of_alpha];
      endfor
      lin_eq_sys_A = [lin_eq_sys_A; sum_of_alpha, (-1)^(i-1)];
      lin_eq_sys_B = [lin_eq_sys_B; f_x(i)];
    endfor
    # Solving linear system
    vector_alphas_sigma = lin_eq_sys_A \ lin_eq_sys_B;
    # disp(vector_alphas_sigma);

    new_pol = vector_alphas_sigma(1:end-1);
    sigma = vector_alphas_sigma(end);

    points_for_plot = -3:0.2:4;
    #
    figure
      grid on
      grid minor on
      hold on
      title("Lab 3: Remez Algorithm");
      # FUNC
      plot(points_for_plot, 600./(points_for_plot.^2 -6*points_for_plot+36),'linewidth', 2, 'color', 'blue');
      # Intpol second
      plot(points_for_plot, polyval(new_pol, points_for_plot), '--','linewidth', 2, 'color', 'green');
      
      plot([nodes_x,nodes_x],[-5,30], '-.', 'color', 'black');
      legend("Function", "Cool pol")

    points_for_calc = -3:PERSITION:4;
    r_func = abs(arrayfun(@(x) 600/(x^2-6*x+36), points_for_calc) - polyval(new_pol, points_for_calc));
    [local_max_f_x, local_max_index] = max(r_func);
    local_max_node_x = -3 + local_max_index*(4 - (-3))/columns(r_func);

    epsilon = local_max_f_x - abs(sigma);

    nearest_point_index = dsearchn(nodes_x, local_max_node_x);
    if abs(f_x(nearest_point_index))/f_x(nearest_point_index) == abs(local_max_f_x)/local_max_f_x
      # Replace
      nodes_x(nearest_point_index) = local_max_node_x;
    else # if not same sign 
      if nearest_point_index == 1 # very left point
        # Add to the left and remove very right
        nodes_x = [local_max_node_x; nodes_x];
        nodes_x = nodes_x(1:end-1);
      elseif nearest_point_index == rows(nodes_x) # very right point
        # Add to the right and remove very left
        nodes_x = [nodes_x; local_max_node_x];
        nodes_x = nodes_x(2:end);
      else # somewhare in the center
        # Find other nearest and replace
        search_scope = [nodes_x(nearest_point_index+1); nodes_x(nearest_point_index-1)];
        finded_index = dsearchn(search_scope, local_max_node_x);
        if finded_index == 1
          # Replace left node
          nodes_x(nearest_point_index+1) = local_max_node_x;
        elseif finded_index == 2
          # Replace right node
          nodes_x(nearest_point_index-1) = local_max_node_x;
        endif
        
      endif
    endif


    points_for_plot = -3:0.01:4;

    figure
      grid on
      grid minor on
      hold on
      title("Lab 3: Remez Algorithm, R-function");
      plot(points_for_plot, arrayfun(@(x) 600/(x^2-6*x+36), points_for_plot) - polyval(new_pol, points_for_plot),'linewidth', 3, 'color', 'blue');
      plot(points_for_plot, abs(arrayfun(@(x) 600/(x^2-6*x+36), points_for_plot) - polyval(new_pol, points_for_plot)),'linewidth', 2, 'color', 'red');
      top_point = [];
      if POL_DEGREE == 5
        top_point = 0.03;
      elseif POL_DEGREE == 10
        top_point = 0.0001;
      endif
      plot([local_max_node_x,local_max_node_x],[-top_point,top_point], '-.', 'linewidth', 1, 'color', 'black');
      plot([-3,4],[local_max_f_x,local_max_f_x], '-.', 'linewidth', 1, 'color', 'black');
      
    # disp(epsilon);
  endwhile

  printf("Cool %d-degree polynomial is: \n", POL_DEGREE);
  disp(new_pol);

endfor
