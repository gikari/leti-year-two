#! /usr/bin/octave
clear; # clc;

POINTS_TO_GEN = 10;

# Points random generation
random_points_x = POINTS_TO_GEN*rand(POINTS_TO_GEN, 1);
random_points_y = POINTS_TO_GEN*rand(POINTS_TO_GEN, 1) + 10;

POL_DEGS = 1:5;
R_arr = [];

for n = POL_DEGS # Polynomial degree
  #
  # APPROX. POLYNOMIALS
  #
  # Ax = b
  # A = G
  # x = pol. coef. (inversed order)
  # b = dot product of f and phi
  A = [];
  b = [];
  
  for i = 0:n
    add_to_A = [];
    for j = 0:n
      add_to_A = [add_to_A, (random_points_x .^i ) .* (random_points_x .^j )];
    endfor
    A = [A; add_to_A];
    b = [b; random_points_y .* (random_points_x .^i ) ];
  endfor
  
  polynomial = flipud(A\b);
  
  PFP = -1+min(random_points_x):0.1:max(random_points_x) + 1;
  
  figure
  grid on
  grid minor on
  hold on
  plot(PFP, polyval(polynomial, PFP), "linewidth", 2, "color", "black");
  plot(random_points_x, random_points_y, "*", "linewidth", 3, "color", "blue");
  title("Lab 4: Lesser Squares - Approx. pol.");
  
  # 
  #  CONVERGENCE
  #
  R = abs(random_points_y - polyval(polynomial, random_points_x) );
  R_arr = [R_arr, R];
  
endfor

figure
  grid on
  grid minor on
  hold on
  for R_row = R_arr'
    plot(POL_DEGS, R_row, "linewidth", 2, "--");
  endfor
  title("Lab 4: Lesser Squares - Convergence.");
  
