#!/usr/bin/octave
#FUNC = 2*cos(2*x + 1)
clear; clc;
#X = [2; 4; 6; 8; 10;];
X = (2:1:10)';
#F_X = [0.5673; -1.8223; 1.8149; -0.5503; -1.0955;];
F_X = arrayfun(@(x) 2*cos(2*x + 1), X);

POINTS_FOR_PLOT = 1:0.1:13;

figure
  plot(POINTS_FOR_PLOT, 2*cos(2*POINTS_FOR_PLOT + 1));
  hold on
  plot(POINTS_FOR_PLOT, lagrange_pol(POINTS_FOR_PLOT, X, F_X));
  title("Lagrange topcheck!");
  legend("Function","Lagrange");
  #hold on
  #plot(X, F_X)  

#FUNC(6) = -128*cos(1 + 2 x) ### d^6 y/ dx^6
RANDOM_POINT = 3; 
MAX_F_X = 128;
SIX_FACT = factorial(rows(X)+1);
OMEGA_5 = omega_n(RANDOM_POINT, X);
MAX_R = abs( OMEGA_5*(MAX_F_X/SIX_FACT) );
EXACT_R = abs( 2*cos(2*RANDOM_POINT + 1) - lagrange_pol(RANDOM_POINT, X, F_X) );

disp("Maximum R: ");
disp(MAX_R);
disp("Exact R: ");
disp(EXACT_R);
