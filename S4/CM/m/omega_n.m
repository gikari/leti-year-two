function [retval] = omega_n (x, X)
  retval = 1;
  for xi = X'
    retval = retval*(x - xi);
  endfor
endfunction
