## Copyright (C) 2018 Mikhail
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## Author: Mikhail <zomial@zomial-TP500LA>
## Created: 2018-03-02

function [retval] = newton_pol (x_var, X, F_X)
  retval = [];
  X_SIZE = rows(X);
  DELTA_X = X(2) - X(1);
  for x_var_elem = x_var
    NEWTON_FORMULA_SUM = 0;
    for order_of_term = 0:X_SIZE-1; 
      NEWTON_FORMULA_PRODUCT = 1;
      NEWTON_FORMULA_PRODUCT *= delta_fx(1, F_X, order_of_term);
      NEWTON_FORMULA_PRODUCT /= factorial(order_of_term);
      NEWTON_FORMULA_PRODUCT /= (DELTA_X)^order_of_term;
      for n = 1:order_of_term
        NEWTON_FORMULA_PRODUCT *= (x_var_elem - X(n));
      endfor
      NEWTON_FORMULA_SUM += NEWTON_FORMULA_PRODUCT;    
    endfor
    retval = [retval; NEWTON_FORMULA_SUM];
  endfor
  
endfunction
