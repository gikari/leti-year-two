## Copyright (C) 2018 Mikhail
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## Author: Mikhail <zomial@zomial-TP500LA>
## Created: 2018-03-02

function [retval] = delta_fx (F_INDEX, F_X, order)
  retval = 0;
  if order > 0
    retval = delta_fx(F_INDEX+1, F_X, order-1) - delta_fx(F_INDEX, F_X, order-1); 
  else
    retval = F_X(F_INDEX);
  endif
endfunction
