function [retval] = lagrange_pol (x_var, X, F_X)
  retval = [];
  X_SIZE = rows(X);
  #disp("SIZE OF X");
  #disp(X_SIZE);
  
  for x_var_elem = x_var
    LAGRANGE_FORMULA_SUM = 0;
    for k = 1:X_SIZE
      LAGRANGE_FORMULA_PRODUCT = 1;
      for xi = X'
        if (xi != X(k))
          LAGRANGE_FORMULA_PRODUCT *= ((x_var_elem - xi)/(X(k) - xi));
        endif
      endfor
      LAGRANGE_FORMULA_SUM += F_X(k) * LAGRANGE_FORMULA_PRODUCT;
    endfor
    retval = [retval; LAGRANGE_FORMULA_SUM];
  endfor
  
endfunction;
