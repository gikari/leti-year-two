#ifndef SHAPE_H
#define SHAPE_H

#include <iostream>
#include "screen.h"


struct shape {
    static shape* list;
    shape* next;
    shape() { next = list; list = this; }
    virtual point north() const = 0;
    virtual point south() const = 0;
    virtual point east() const = 0;
    virtual point west() const = 0;
    virtual point neast() const = 0;
    virtual point seast() const = 0;
    virtual point nwest() const = 0;
    virtual point swest() const = 0;
    virtual void draw() = 0;
    virtual void move(int, int) = 0;
    virtual void resize(int) = 0;
};

class rotatable : virtual public shape {
public:
    virtual void rotate_left() = 0;
    virtual void rotate_right() = 0;
};

class reflectable : virtual public shape {
public:
    virtual void flip_horizontally() = 0;
    virtual void flip_vertically() = 0;
};

class line : public shape {
protected:
    point w, e;
public:
    line(point a, point b) : w(a), e(b) {};
    line(point a, int L) : w(point(a.x + L - 1, a.y)), e(a) {};
    point north() const { return point((w.x + e.x) / 2, e.y < w.y ? w.y : e.y); }
    point south() const { return point((w.x + e.x) / 2, e.y < w.y ? e.y : w.y); }
    point east() const { return point((w.x + e.x) / 2, e.y < w.y ? e.y : w.y); }
    point west() const { return point((w.x + e.x) / 2, e.y < w.y ? e.y : w.y); }
    point neast() const { return point((w.x + e.x) / 2, e.y < w.y ? e.y : w.y); }
    point seast() const { return point((w.x + e.x) / 2, e.y < w.y ? e.y : w.y); }
    point nwest() const { return point((w.x + e.x) / 2, e.y < w.y ? e.y : w.y); }
    point swest() const { return point((w.x + e.x) / 2, e.y < w.y ? e.y : w.y); }
    void move(int a, int b) { w.x += a; w.y += b; e.x += a; e.y += b; }
    void draw() {
        try {
            put_line(w, e);
        } catch (out_of_screen_exception e) {
            std::cerr << e.what() << "\n";
        }
    }
    void resize(int d) { e.x += (e.x - w.x)*(d - 1); e.y += (e.y - w.y)*(d - 1); }
};

class rectangle :public rotatable {
protected:
    point sw, ne;
public:
    rectangle(point, point);
    point north() const { return point((sw.x + ne.x) / 2, ne.y); }
    point south() const { return point((sw.x + ne.x) / 2, sw.y); }
    point east() const { return point(sw.x, (sw.y + ne.y) / 2); }
    point west() const { return point(ne.x, (sw.y + ne.y) / 2);}
    point neast() const { return ne; }
    point seast() const { return point(ne.x, sw.y); }
    point nwest() const { return point(sw.x, ne.y); }
    point swest() const { return sw; }
    void rotate_right() { int w = ne.x - sw.x, h = ne.y - sw.y; sw.x = ne.x - h * 2; ne.y = sw.y + w / 2; }
    void rotate_left() { int w = ne.x - sw.x, h = ne.y - sw.y; ne.x = sw.x + h * 2; ne.y = sw.y + w / 2; }
    void move(int a, int b) { sw.x += a; sw.y += b; ne.x += a; ne.y += b; }
    void resize(int d) { ne.x += (ne.x - sw.x)*(d - 1); ne.y += (ne.y - sw.y)*(d - 1); }
    void draw();
};


void shape_refresh();
void up(shape& p, const shape& q);
void down(shape &p, shape &q);
void match_nw(shape &p, shape &q);
void match_ne(shape &p, shape &q);

//Дополнительная "сборная" фигура
class myshape : public rectangle {
    int w, h;
    line l_eye;
    line r_eye;
    line mouth;
public:
    myshape(point, point);
    void draw();
    void move(int, int);
    void resize(int) {}
};

//Пример: дополнительный фрагмент
class h_circle : public rectangle, public reflectable {
    bool reflected;
public:
    h_circle(point a, point b, bool r = true) :rectangle(a, b), reflected(r) {}
    void draw();
    void flip_horizontally() {};
    void flip_vertically() { reflected = !reflected; };
};


class cross_rectangle : public rectangle {
public:
    cross_rectangle(point a, point b);
    void draw();
};


#endif // SHAPE_H
