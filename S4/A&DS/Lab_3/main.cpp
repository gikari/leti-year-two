/*
 * (A or B xor C and D) and E
 * Sets as hash-tables
 */
#include <iostream>
#include <string>
#include "Set.h"

int main(int argc, char **argv) {

    std::string set_str;
    try {
        std::cout << "Enter set A: ";
        std::getline(std::cin, set_str);
        Set setA{ set_str };
        std::cout << "Enter set B: ";
        std::getline(std::cin, set_str);
        Set setB{ set_str };
        std::cout << "Enter set C: ";
        std::getline(std::cin, set_str);
        Set setC{ set_str };
        std::cout << "Enter set D: ";
        std::getline(std::cin, set_str);
        Set setD{ set_str };
        std::cout << "Enter set E: ";
        std::getline(std::cin, set_str);
        Set setE{ set_str };
        Set set_result{};
        set_result = ((setA | setB) + (setC & setD)) & setE;
        setA.show();
        setB.show();
        setC.show();
        setD.show();
        setE.show();
        std::cout << "Resulting set: " << std::endl;
        set_result.show();
    }
    catch (const not_represents_hex_error& ) {
        std::cerr << "Char don't represents hex digit!" << std::endl;
    }
    catch (const std::overflow_error&) {
        std::cerr << "Too many elements in set!" << std::endl;
    }
    catch (const std::out_of_range&) {
        std::cerr << "Hash table error: Requested hash out of range!" << std::endl;
    }
    catch (const element_not_found&) {
        std::cerr << "Hash table error: element not found!" << std::endl;
    }
    catch (const std::runtime_error&) {
        std::cerr << "Unknown error!" << std::endl;
    }

    return 0;
}
