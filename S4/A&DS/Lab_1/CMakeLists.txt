cmake_minimum_required(VERSION 2.6)
project(lab_1)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)


add_executable(lab_1 main.cpp screen.cpp shape.cpp)

install(TARGETS lab_1 RUNTIME DESTINATION bin)
