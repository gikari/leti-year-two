#include "shape.h"
#include "screen.h"

shape* shape::list = nullptr;

void h_circle::draw()
{
    int x0 = (sw.x + ne.x) / 2;
    int y0 = reflected ? sw.y : ne.y;
    int radius = (ne.x - sw.x) / 2;
    int x = 0;
    int y = radius;
    int delta = 2 - 2 * radius;
    int error = 0;
    while (y >= 0) {
        if (reflected) { put_point(x0 + x, y0 + y*0.7); put_point(x0 - x, y0 + y*0.7); }
        else { put_point(x0 + x, y0 - y*0.7); put_point(x0 - x, y0 - y*0.7); }
        error = 2 * (delta + y) - 1;
        if (delta < 0 && error <= 0) { ++x; delta += 2 * x + 1; continue; }
        error = 2 * (delta - x) - 1;
        if (delta > 0 && error > 0) { --y; delta += 1 - 2 * y; continue; }
        ++x; delta += 2 * (x - y); --y;
    }
}

void down(shape &p, shape &q)
{
    point n = q.south();
    point s = p.north();
    p.move(n.x - s.x, n.y - s.y - 1);
}

void up(shape& p, const shape& q)
{
    point n = q.north();
    point s = p.south();
    p.move(n.x - s.x, n.y - s.y + 1);
}

void match_ne(shape& p, shape& q)
{
    point n = q.neast();
    point s = p.neast();
    std::cout<< n.x << "; " << n.y << std::endl;
    std::cout<< s.x << "; " << s.y << std::endl;
    p.move(n.x - s.x, n.y - s.y);
    std::cout<< q.neast().x << "; " << q.neast().y << std::endl;
    std::cout<< p.neast().x << "; " << p.neast().y << std::endl;
}

void match_nw(shape& p, shape& q)
{
    point n = q.nwest();
    point s = p.nwest();
    std::cout<< n.x << "; " << n.y << std::endl;
    std::cout<< s.x << "; " << s.y << std::endl;
    p.move(n.x - s.x, n.y - s.y);
    std::cout<< q.nwest().x << "; " << q.nwest().y << std::endl;
    std::cout<< p.nwest().x << "; " << p.nwest().y << std::endl;
}


void shape_refresh()
{
    screen_clear();
    for (shape* p = shape::list; p; p = p->next)
        p->draw();
    screen_refresh();
}

rectangle::rectangle(point a, point b)
{
    if (a.x <= b.x) {
        if (a.y <= b.y) sw = a, ne = b;
        else sw = point(a.x, b.y), ne = point(b.x, a.y);
    }
    else {
        if (a.y <= b.y) sw = point(b.x, a.y), ne = point(a.x, b.y);
        else sw = b, ne = a;
    }
}

void rectangle::draw()
{
    put_line(nwest(), ne);
    put_line(ne, seast());
    put_line(seast(), sw);
    put_line(sw, nwest());
}


myshape::myshape(point a, point b)
: rectangle(a, b),
w(neast().x - swest().x + 1),
h(neast().y - swest().y + 1),
l_eye(point(swest().x + 2, swest().y + h * 3 / 4), 2),
r_eye(point(swest().x + w - 4, swest().y + h * 3 / 4), 2),
mouth(point(swest().x + 2, swest().y + h / 4), w - 4)
{

}

void myshape::draw()
{
    rectangle::draw();
    int a = (swest().x + neast().x) / 2;
    int b = (swest().y + neast().y) / 2;
    put_point(point(a, b));
}

void myshape::move(int a, int b)
{
    rectangle::move(a, b);
    l_eye.move(a, b);
    r_eye.move(a, b);
    mouth.move(a, b);
}

cross_rectangle::cross_rectangle (point a, point b) :
rectangle(a,b)
{};

void cross_rectangle::draw()
{
    rectangle::draw();
    put_line(north(), south());
    put_line(east(), west());
}


