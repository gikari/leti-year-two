#ifndef SCREEN_H
#define SCREEN_H

const int XMAX = 80;
const int YMAX = 40;

extern char screen[YMAX][XMAX];
enum color {black='*',white='_'};

class point {
public:
    int x, y;
    explicit point(int a = 0, int b = 0) : x(a), y(b) {}
};

void put_point(int a, int b);
void put_point(point p);

void put_line(int, int, int, int);
void put_line(point a, point b);

extern void screen_init();
extern void screen_destroy();
extern void screen_refresh();
extern void screen_clear();

#endif
