#include "screen.h"
#include "shape.h"
#include <iostream>

int main()
{
    screen_init();
    rectangle p1(point(0, 0), point(14, 5));
    line p2(point(0, 15), 17);
    myshape p3(point(15, 10), point(27, 18));
    //h_circle p4(point(40, 10), point(50, 20));
    cross_rectangle p5(point(0,0), point(6,4));
    p1.resize(2);
    p2.resize(2);
    p3.resize(2);
//     p5.resize(2);
    p1.rotate_right();
    p2.resize(2);
    p3.move(10, 0);
    up(p2, p3);
    up(p1, p2);
    down(p5, p3);
    shape_refresh();
    std::cout << "=== Ready! Next pos... ===\n";
    std::cin.get();
    //p3.resize(2);
    match_ne(p5, p3);
    shape_refresh();
    std::cout << "=== Ready! Next pos... ===\n";
    std::cin.get();
    match_nw(p5, p3);
    shape_refresh();
    std::cout << "=== Ready! ===\n";
    std::cin.get();

    screen_destroy();
    return 0;
}

