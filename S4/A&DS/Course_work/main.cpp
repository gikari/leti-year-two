#include <chrono>
#include <fstream>
#include <iostream>
#include <random>
#include <string>
#include <sstream>
#include "stud.h"

template <typename T>
void dbg_msg(const T& msg, bool debug_on) {
    if (debug_on) {
        std::cout << msg << std::endl;
    }
};

int main() {
    static const bool SHOW_DEBUG_MESSAGES = true;
    static const int START_POWER = 10;
    static const int END_POWER = 210;
    static const int STEP = 5;
    static const int NUMBER_OF_TESTS = (END_POWER - START_POWER) / STEP + 1;

    // OPERATIONS:
    // Insert N-length sequence into the center of N-length sequence
    // Then merge it with sequence of length N

    // N ranges from 10 to 200
    std::random_device rd;
    std::mt19937 mt{rd()};
    std::uniform_int_distribution<int> dist (0, 15);

    dbg_msg(NUMBER_OF_TESTS, SHOW_DEBUG_MESSAGES);
    std::ofstream fs {"../in.txt", std::ios::out};
    fs << NUMBER_OF_TESTS << "\n";
    fs.close();

    long least_time = 0;
    for (int len_of_seq = START_POWER; len_of_seq <= END_POWER; len_of_seq += STEP) {
        stud::Sequence seq_to_be_inserted_into{};
        stud::Sequence seq_to_be_inserted{};
        stud::Sequence seq_to_be_merged_with{};

        for (int i = 0; i < len_of_seq; ++i) {
            seq_to_be_inserted_into.emplace_back(dist(mt));
            seq_to_be_inserted.emplace_back(dist(mt));
            seq_to_be_merged_with.emplace_back(dist(mt));
        }

        stud::Sequence resulting_sequence {seq_to_be_inserted_into};

        auto start_time = std::chrono::high_resolution_clock::now();

        resulting_sequence = stud::insert(resulting_sequence, seq_to_be_inserted, len_of_seq/2);
        resulting_sequence = stud::merge(resulting_sequence, seq_to_be_merged_with);

        auto end_time = std::chrono::high_resolution_clock::now();
        auto execution_time = end_time = start_time;
        auto exec_time_ns = std::chrono::time_point_cast<std::chrono::microseconds>(execution_time);
        long exec_time_formated = exec_time_ns.time_since_epoch().count() - least_time;

        if (len_of_seq == START_POWER) {
            least_time = exec_time_formated;
            exec_time_formated -= least_time;
        }


        std::stringstream ss;
        // ss << "[ " << len_of_seq << ", " << exec_time_formated << " ]";
        ss << len_of_seq << " " << exec_time_formated;

        dbg_msg(ss.str(), SHOW_DEBUG_MESSAGES);
        std::ofstream fs {"../in.txt", std::ios::app};
        ss << "\n";
        fs << ss.str();
        fs.close();

        // dbg_msg(resulting_sequence, SHOW_DEBUG_MESSAGES);


    }

    std::cout << "Done!\n";
    return 0;
}
