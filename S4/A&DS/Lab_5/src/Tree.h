#ifndef TREE_H
#define TREE_H

#include <vector>
#include <array>
#include <memory>

namespace stud
{

class B23_Node
{
public:
    B23_Node();
    B23_Node(std::unique_ptr<int>);
    B23_Node ( const std::vector<int>& );
    ~B23_Node() = default;
    B23_Node ( const B23_Node& );
    B23_Node ( B23_Node&& ) noexcept;
    B23_Node& operator = ( const B23_Node& );
    B23_Node& operator = ( B23_Node&& ) noexcept;

    void add ( int );

    const B23_Node* find ( int ) const;
    const int* find_concrete_value (int) const;
    //B23_Node* find ( int );
    std::vector<int> get_array_representation() const;
    int capacity() const;
    void show() const;

private:
    bool node_is_empty() const;
    bool node_has_only_one_element() const;
    bool node_has_two_elements() const;
    bool node_is_leaf() const;

    void add (std::unique_ptr<int>);
    void add_as_to_leaf ( std::unique_ptr<int> );
    void add_as_to_parent ( std::unique_ptr<int> );
    void split_leaf (std::unique_ptr<int> );
    void push_up_value_and_connect_its_children( std::unique_ptr<int> , std::unique_ptr<B23_Node> , std::unique_ptr<B23_Node> );
    void split_node_and_connect_previous_children ( std::unique_ptr<int>, std::unique_ptr<B23_Node> , std::unique_ptr<B23_Node> );
    void connect_children();


    std::array <std::unique_ptr<int>, 2> data;
    std::array <std::unique_ptr<B23_Node>, 3> children;
    B23_Node* parent;
};


class B23_Tree {
public:
    B23_Tree() : root{std::make_unique<B23_Node>()} {};
    B23_Tree ( const std::vector<int>& vec) : root{} {
        root = std::make_unique<B23_Node>(vec);
    };
    ~B23_Tree() = default;
    B23_Tree ( const B23_Tree& rhs) : root{ std::make_unique<B23_Node>(*(rhs.root)) } {};
    B23_Tree ( B23_Tree&& rhs) noexcept : root{} {
        root = std::move(rhs.root);
    };
    B23_Tree& operator = ( const B23_Tree& rhs) {
        if (this != &rhs)
        {
            root = std::make_unique<B23_Node>(*(rhs.root));
        }
        return *this;
    };
    B23_Tree& operator = ( B23_Tree&& rhs) noexcept {
        if (this != &rhs)
        {
            root = std::move(rhs.root);
        }
        return *this;
    };

    void add (int val) {
        root->add(val);
    };

    const B23_Node* find (int val) const{
        return root->find(val);
    };
    const int* find_concrete_value (int val) const {
        return root->find_concrete_value(val);
    };
    std::vector<int> get_array_representation() const {
        return root->get_array_representation();
    };
    int capacity() const {
        return root->capacity();
    };
    void show() const {
        root->show();
    };
private:
    std::unique_ptr<B23_Node> root;
};

}

#endif
