/*
 * (A or B xor C and D) and E
 * Sets as hash-tables
 *
 */
#include <iostream>
#include <vector>
#include "Tree.h"
#include "Sequence.h"
#include <cstring>
#include "Set.h"

int main()
{
    using namespace stud;
    using namespace std;

    std::string string1{};
    std::string string2{};
    cout << "Enter first sequence: ";
    std::getline(cin, string1);
    cout << "Enter second sequence: ";
    std::getline(cin, string2);
    Sequence seq1{string1};
    Sequence seq2{string2};
    cout << "Choose sequence operation (C - Concat; E - Excl; S - Subst) : ";
    char operation = getchar();
    if (operation == 'C' || operation == 'c') {
        seq1.concat(seq2);
    } else if (operation == 'E' || operation == 'e') {
        seq1.excl(seq2);
    } else if (operation == 'S' || operation == 's') {
        cout << "Choose insert position" << endl;
        int position;
        cin >> position;
        seq1.insert(seq2, position);
    }
    seq1.show();

    std::cout << "Press any key to do nothing..." << endl;
    // system("pause");
    return 0;
}
