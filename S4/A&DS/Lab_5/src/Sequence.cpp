/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright 2018  Genda Ikari <email>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Sequence.h"
#include "Set.h"
#include <iostream>
#include <algorithm>

stud::Sequence::Sequence(const std::string& str) : values_set{}, values_order{}
{
    for (auto ch : str) {
        add(Set::char_to_int(ch));
    }
}


using namespace stud;

namespace stud {

Sequence::Sequence(const std::vector<int>& vec) : values_set{}, values_order{}
{
    for (auto elem : vec) {
        add(elem);
    }
}


Sequence::Sequence(const Sequence& other) : values_set{other.values_set}, values_order{other.values_order}
{}

Sequence& Sequence::operator=(const Sequence& other)
{
    if (this != &other)
    {
        values_set = other.values_set;
        values_order = other.values_order;
    }
    return *this;
}

Sequence::Sequence(stud::Sequence && other) noexcept : values_set{std::move(other.values_set)}, values_order{std::move(other.values_order)}
{
}

Sequence & stud::Sequence::operator=(stud::Sequence && other) noexcept
{
    if (this != &other)
    {
        values_set = std::move(other.values_set);
        values_order = std::move(other.values_order);
    }
    return *this;
}


void Sequence::add(int val)
{
    values_set.add(val);
    values_order.push_back(values_set.find_concrete_value(val));
}

std::vector<int> Sequence::get_vector_representation() const {
    std::vector<int> result{};
    for (auto elem : values_order) {
        result.push_back(*elem);
    }
    return result;
}

void Sequence::concat (const Sequence& other){
    auto seq_to_concat = other.get_vector_representation();
    auto concated_sequence  = get_vector_representation();
    concated_sequence.reserve(concated_sequence.size() + seq_to_concat.size());
    concated_sequence.insert(concated_sequence.end(), seq_to_concat.begin(), seq_to_concat.end());
    *this = Sequence{concated_sequence};
}

void Sequence::show() {
    std::cout << to_str() << std::endl;
}

}

void stud::Sequence::excl(const stud::Sequence& other)
{
    auto resulting_seq = get_vector_representation();
    auto seq_to_excl = other.get_vector_representation();
    auto pos_to_excl = std::search(resulting_seq.begin(), resulting_seq.end(), seq_to_excl.begin(), seq_to_excl.end());

    if (pos_to_excl != resulting_seq.end()) {
        auto end_iter_of_first_seq = (pos_to_excl == resulting_seq.begin()) ? resulting_seq.begin() : pos_to_excl;
        std::vector<int> first_part_of_new_seq {resulting_seq.begin(), end_iter_of_first_seq};
        std::vector<int> second_part_of_new_seq {pos_to_excl + seq_to_excl.size(), resulting_seq.end()};
        first_part_of_new_seq.insert(first_part_of_new_seq.end(), second_part_of_new_seq.begin(), second_part_of_new_seq.end());
        *this = Sequence{first_part_of_new_seq};
    } else {
        std::cerr << "Excluding sequence not found!\n";
    }
}

void stud::Sequence::insert(const stud::Sequence& other, size_t position)
{
    auto resulting_seq = get_vector_representation();
    auto seq_to_incl = other.get_vector_representation();
    if (position > 0 && position <= resulting_seq.size()) {
        std::vector<int> first_part_of_new_seq {resulting_seq.begin(), resulting_seq.begin()+position};
        std::vector<int> third_part_of_new_seq {resulting_seq.begin()+position, resulting_seq.end()};
        first_part_of_new_seq.insert(first_part_of_new_seq.end(), seq_to_incl.begin(), seq_to_incl.end());
        first_part_of_new_seq.insert(first_part_of_new_seq.end(), third_part_of_new_seq.begin(), third_part_of_new_seq.end());
        *this = Sequence{first_part_of_new_seq};
    } else if (position == 0) {
        seq_to_incl.insert(seq_to_incl.end(), resulting_seq.begin(), resulting_seq.end());
        *this = Sequence{seq_to_incl};
    } else {
        std::cerr << "Requested position is out of range!\n";
    }
}

bool stud::Sequence::operator==(const stud::Sequence& other) const
{
    auto this_seq = get_vector_representation();
    auto other_seq = other.get_vector_representation();
    return this_seq == other_seq;
}

std::string stud::Sequence::to_str()
{
    std::string str_repr{};
    for (auto val : values_order) {
        str_repr += Set::int_to_char(*val);
    }
    return str_repr;
}

void stud::Sequence::merge(const stud::Sequence& rhs)
{
    Sequence new_one{*this};
    new_one.concat(rhs);
    show();
    new_one.sort();
    show();
    *this = Sequence{new_one};
    show();
}

void stud::Sequence::sort()
{
    for (auto it_i = values_order.begin(); it_i != values_order.end(); it_i++) {
        for (auto it_j = it_i; it_j != values_order.end(); it_j++) {
            if (**it_i > **it_j) {
                std::iter_swap(it_i, it_j);
            }
        }
    }
}



/*
bool stud::Sequence::operator==(const std::string& other_str) const
{
    auto this_seq = get_vector_representation();
    return Sequence{this_seq} == Sequence{other_str};
}
*/
