/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright 2018  Genda Ikari <email>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STUD_SEQUENCE_H
#define STUD_SEQUENCE_H

#include <list>
#include <vector>
#include <string>
#include "Tree.h"

namespace stud
{
class Sequence
{
public:
    Sequence () = default;
    Sequence (const std::vector<int>&);
    Sequence (const std::string&);
    ~Sequence() = default;
    Sequence ( const Sequence& );
    Sequence ( Sequence&& ) noexcept;
    Sequence& operator = ( const Sequence& );
    Sequence& operator = ( Sequence && ) noexcept;
    bool operator == (const Sequence&) const;
 //   bool operator == (const std::string&) const;

    void add ( int );
    void concat ( const Sequence& );
    void excl ( const Sequence& );
    void insert ( const Sequence&, size_t position );
    void merge(const Sequence&);
    void sort();

    std::vector<int> get_vector_representation() const;
    void show ();
    std::string to_str();

private:
    B23_Tree values_set;
    std::list<const int*> values_order;
};

}

#endif
