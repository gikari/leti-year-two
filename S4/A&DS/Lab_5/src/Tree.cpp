#include "Tree.h"
#include <vector>
#include <stdexcept>
#include <iostream>
#include <memory>
#include <utility>

namespace stud
{

B23_Node::B23_Node()
: data{},
  children{},
  parent {}
{}

B23_Node::B23_Node(std::unique_ptr<int> val)
: data{},
  children{},
  parent {}
{
    data[0] = std::move(val);
}

B23_Node::B23_Node(const B23_Node& another)
{
    for (int i {0}; i < 2; i++)
        data[i] = another.data[i] ? std::make_unique<int>(*(another.data[i])) : nullptr;

    for (int i {0}; i < 3; i++)
        children[i] = another.children[i] ? std::make_unique<B23_Node>(*(another.children[i])) : nullptr;



    connect_children();
}

B23_Node::B23_Node(B23_Node&&  another) noexcept
: data{std::move(another.data)},
  children{std::move(another.children)},
  parent {another.parent}
{
    another.parent = nullptr;
}

B23_Node::B23_Node(const std::vector<int>& array)
: data{},
  children{},
  parent {}
{
    for (int elem : array)
    {
        add(elem);
        std::cout << "Added " << elem << std::endl;
    }
}

B23_Node& B23_Node::operator=(const B23_Node& another)
{
    if (this != &another) {
        for (auto i = 0; i < 2; i++)
            data[i] = another.data[i] ? std::make_unique<int>(*(another.data[i])) : nullptr;

        for (auto i = 0; i < 3; i++)
            children[i] = another.children[i] ? std::make_unique<B23_Node>(*(another.children[i])) : nullptr;



        connect_children();
    }
    return *this;
}

B23_Node& B23_Node::operator=(B23_Node && another) noexcept
{
    if (this != &another) {
        data = std::move(another.data);
        children = std::move(another.children);
        parent = another.parent;
        another.parent = nullptr;
    }
    return *this;
}


void B23_Node::add(int elem_to_add)
{
    auto new_elem_ptr = std::make_unique<int>(elem_to_add);
    add(std::move(new_elem_ptr));
}

void B23_Node::add(std::unique_ptr<int> new_elem)
{
    if (node_is_leaf()) {
        add_as_to_leaf(std::move(new_elem));
    } else {
        add_as_to_parent(std::move(new_elem));
    }
}

void B23_Node::add_as_to_parent(std::unique_ptr<int> new_elem)
{
    if (node_is_empty()) {
        throw std::runtime_error("Empty node, that have child!");
    } else if (node_has_only_one_element()) {
        if (*new_elem > *data[0]) {
           children[1]->add(std::move(new_elem));
        } else if (*new_elem <= *data[0]) {
            children[0]->add(std::move(new_elem));
        }
    } else if (node_has_two_elements()) {
        if (*new_elem < *data[0]) {
            children[0]->add(std::move(new_elem));
        } else if (*new_elem > *data[0] && *new_elem < *data[1]) {
            children[1]->add(std::move(new_elem));
        } else if (*new_elem > *data[1]) {
           children[2]->add(std::move(new_elem));
        }
    }
}


void B23_Node::add_as_to_leaf(std::unique_ptr<int> new_elem)
{
    if (node_is_empty()) {
        data[0] = std::move(new_elem);
    } else if (node_has_only_one_element()) {
        if (*new_elem > *data[0]) {
            data[1] = std::move(new_elem);
        } else if (*new_elem <= *data[0]) {
            data[1] = std::move(data[0]);
            data[0] = std::move(new_elem);
        }
    } else if (node_has_two_elements()) {
        split_leaf(std::move(new_elem));
    }
}

void B23_Node::split_leaf(std::unique_ptr<int> new_elem)
{
    std::unique_ptr<int> value_to_push_up_through_tree {};
    std::unique_ptr<int> value_for_left_node {};
    std::unique_ptr<int> value_for_right_node {};
    if (*new_elem <= *data[0]) {
        value_to_push_up_through_tree = std::move(data[0]);
        value_for_left_node = std::move(new_elem);
        value_for_right_node = std::move(data[1]);
    } else if (*new_elem > *data[0] && *new_elem <= *data[1]) {
        value_to_push_up_through_tree = std::move(new_elem);
        value_for_left_node = std::move(data[0]);
        value_for_right_node = std::move(data[1]);
    } else if (*new_elem > *data[1]) {
        value_to_push_up_through_tree = std::move(data[1]);
        value_for_left_node = std::move(data[0]);
        value_for_right_node = std::move(new_elem);
    }

    auto new_left_node = std::make_unique<B23_Node>(std::move(value_for_left_node));
    auto new_right_node = std::make_unique<B23_Node>(std::move(value_for_right_node));

    if (parent) {
        push_up_value_and_connect_its_children(std::move(value_to_push_up_through_tree), std::move(new_left_node), std::move(new_right_node));
        return;
    } else {
        data[0] = std::move(value_to_push_up_through_tree);
        data[1] = nullptr;
        children[0] = std::move(new_left_node);
        children[1] = std::move(new_right_node);
        this->connect_children();
    }

}

void B23_Node::push_up_value_and_connect_its_children(std::unique_ptr<int> new_elem, std::unique_ptr<B23_Node> left_elem_child, std::unique_ptr<B23_Node> right_elem_child)
{
    auto parent_node = this->parent;
    if (!parent_node) {
        throw std::runtime_error("Trying to push value from node with no parent!");
    }
    if (parent_node->node_has_only_one_element()) {
        if (*new_elem > *(parent_node->data[0])) {
            parent_node->data[1] = std::move(new_elem);
            parent_node->children[1] = std::move(left_elem_child);
            parent_node->children[2] = std::move(right_elem_child);
        } else if (*new_elem <= *(parent_node->data[0])) {
            parent_node->data[1] = std::move(parent_node->data[0]);
            parent_node->data[0] = std::move(new_elem);
            parent_node->children[2]= std::move(parent_node->children[1]);
            parent_node->children[1] = std::move(right_elem_child);
            parent_node->children[0] = std::move(left_elem_child);
        }
    } else if (parent_node->node_has_two_elements()) {
        parent_node->split_node_and_connect_previous_children(std::move(new_elem), std::move(left_elem_child), std::move(right_elem_child));
    }
    parent_node->connect_children();
}

void B23_Node::split_node_and_connect_previous_children(std::unique_ptr<int> new_elem, std::unique_ptr<B23_Node> prev_left_elem_child, std::unique_ptr<B23_Node> prev_right_elem_child)
{
    std::unique_ptr<int> value_to_push_up_through_tree {};
    std::unique_ptr<int> value_for_left_node {};
    std::unique_ptr<int> value_for_right_node {};

    std::unique_ptr<B23_Node> new_left_child_of_left_node {};
    std::unique_ptr<B23_Node>  new_center_child_of_left_node {};
    std::unique_ptr<B23_Node> new_left_child_of_right_node {};
    std::unique_ptr<B23_Node>  new_center_child_of_right_node {};

    if (*new_elem <= *data[0]) {
        // CAME FROM LEFT
        value_to_push_up_through_tree = std::move(data[0]);
        value_for_left_node = std::move(new_elem);
        value_for_right_node = std::move(data[1]);

        new_left_child_of_left_node = std::move(prev_left_elem_child);
        new_center_child_of_left_node = std::move(prev_right_elem_child);
        new_left_child_of_right_node = std::move(children[1]);
        new_center_child_of_right_node = std::move(children[2]);
    } else if (*new_elem > *data[0] && *new_elem <= *data[1]) {
        // CAME FROM CENTER
        value_to_push_up_through_tree = std::move(new_elem);
        value_for_left_node = std::move(data[0]);
        value_for_right_node = std::move(data[1]);

        new_left_child_of_left_node = std::move(children[0]);
        new_center_child_of_left_node = std::move(prev_left_elem_child);
        new_left_child_of_right_node = std::move(prev_right_elem_child);
        new_center_child_of_right_node = std::move(children[2]);
    } else if (*new_elem > *data[1]) {
        // CAME FROM RIGHT
        value_to_push_up_through_tree = std::move(data[1]);
        value_for_left_node = std::move(data[0]);
        value_for_right_node = std::move(new_elem);

        new_left_child_of_left_node = std::move(children[0]);
        new_center_child_of_left_node = std::move(children[1]);
        new_left_child_of_right_node = std::move(prev_left_elem_child);
        new_center_child_of_right_node = std::move(prev_right_elem_child);
    }

    auto new_left_node = std::make_unique<B23_Node>(std::move(value_for_left_node));
    auto new_right_node = std::make_unique<B23_Node>(std::move(value_for_right_node));

    new_left_node->children[0] = std::move(new_left_child_of_left_node);
    new_left_node->children[1] = std::move(new_center_child_of_left_node);
    new_right_node->children[0] = std::move(new_left_child_of_right_node);
    new_right_node->children[1] = std::move(new_center_child_of_right_node);

    if (parent) {
        push_up_value_and_connect_its_children(std::move(value_to_push_up_through_tree), std::move(new_left_node), std::move(new_right_node));

        this->data[0] = nullptr;
        this->data[1] = nullptr;
        this->children[0] = nullptr;
        this->children[1] = nullptr;
        this->children[2] = nullptr;
        return;
    } else {
        this->data[0] = std::move(value_to_push_up_through_tree);
        this->data[1] = nullptr;
        this->children[0] = std::move(new_left_node);
        this->children[1] = std::move(new_right_node);
        this->connect_children();
    }
}


bool B23_Node::node_is_empty() const
{
    return !data[0] && !data[1];
}

bool B23_Node::node_has_only_one_element() const
{
    return data[0] && !data[1];
}

bool B23_Node::node_has_two_elements() const
{
    return data[0] && data[1];
}

bool B23_Node::node_is_leaf() const
{
    return !(children[0] || children[1] || children[2]);
}

int B23_Node::capacity() const
{
    int cap {0};
    if (data[0]) {
        cap++;
    }
    if (data[1]) {
        cap++;
    }
    if (children[0]) {
        cap += children[0]->capacity();
    }
    if (children[1]) {
        cap += children[1]->capacity();
    }
    if (children[2]) {
        cap += children[2]->capacity();
    }
    return cap;
}

void B23_Node::show() const
{
    if (data[1] && data[0]) {
        std::cout << "[ " << *data[0] << ", " << *data[1] << " ]" << std::endl;
    } else if (data[0]) {
        std::cout << "[ " << *data[0] << ", nope ]" << std::endl;
    } else {
        std::cout << "No data in node!" << std::endl;
    }
    if (children[0]) {
        children[0]->show();
    }
    if (children[1]) {
        children[1]->show();
    }
    if (children[2]) {
        children[2]->show();
    }
}

void B23_Node::connect_children()
{
    if (this->children[0]) {
        this->children[0]->parent = this;
    }
    if (this->children[1]) {
        this->children[1]->parent = this;
    }
    if (this->children[2]) {
        this->children[2]->parent = this;
    }
}

const B23_Node* B23_Node::find(int value) const
{
    if ((data[0] && (*data[0] == value)) || (data[1] && (*data[1] == value))) {
        return const_cast<const B23_Node*>(this);
    }

    if (data[0]) {
        if (value < *data[0]) {
            if (children[0]) {
                return children[0]->find(value);
            }
        } else if (value > *data[0]) {
            if (data[1] && value > *data[1]) {
                if (children[2]) {
                    return children[2]->find(value);
                }
            } else if ((data[1] && value < *data[1]) || data[1] == nullptr) {
                if (children[1]) {
                    return children[1]->find(value);
                }
            }
        }
    }
    return nullptr;
}
/*
B23_Node* B23_Node::find(int value)
{
    return const_cast<B23_Node*>(const_cast<const B23_Node&>(*this).find(value));
}
*/
std::vector<int> B23_Node::get_array_representation() const
{
    std::vector<int> array {};
    if (data[0]) {
        array.push_back(*data[0]);
    }
    if (data[1]) {
        array.push_back(*data[1]);
    }
    if (children[0]) {
        auto subtree_elements = children[0]->get_array_representation();
        array.reserve(array.size() + subtree_elements.size());
        array.insert(array.end(), subtree_elements.begin(), subtree_elements.end());
    }
    if (children[1]) {
        auto subtree_elements = children[1]->get_array_representation();
        array.reserve(array.size() + subtree_elements.size());
        array.insert(array.end(), subtree_elements.begin(), subtree_elements.end());
    }
    if (children[2]) {
        auto subtree_elements = children[2]->get_array_representation();
        array.reserve(array.size() + subtree_elements.size());
        array.insert(array.end(), subtree_elements.begin(), subtree_elements.end());
    }

    return array;
}

const int* B23_Node::find_concrete_value(int value) const {
    auto founded_node = find(value);
    if (founded_node->data[1]) {
        return *(founded_node->data[0]) == value ? founded_node->data[0].get() : founded_node->data[1].get();
    }
    else
    {
        return founded_node->data[0].get();
    }

}

}


