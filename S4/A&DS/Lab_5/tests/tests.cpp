#include <gtest/gtest.h>
#include "../src/Sequence.h"

TEST(SequenceTest, Concat) {
    using namespace stud;

    Sequence seq_reciever{"1234"};
    Sequence seq_sender{"12345"};
    seq_reciever.concat(seq_sender);

    EXPECT_EQ(seq_reciever, Sequence{"123412345"});

    Sequence seq_rec2{"1"};
    Sequence seq_sen2{""};
    seq_rec2.concat(seq_sen2);

    EXPECT_EQ(seq_rec2, Sequence{"1"});

    Sequence seq_rec3{""};
    Sequence seq_sen3{"A"};
    seq_rec3.concat(seq_sen3);

    EXPECT_EQ(seq_rec3, Sequence{"A"});

    Sequence seq_rec4{"AAA"};
    Sequence seq_sen4{"AAA"};
    seq_rec4.concat(seq_sen4);

    EXPECT_EQ(seq_rec4, Sequence{"AAAAAA"});
}

TEST(SequenceTest, exclude) {
    using namespace stud;

    // From start
    Sequence seq_rec1 {"123456"};
    Sequence seq_sender{"12345"};
    seq_rec1.excl(seq_sender);

    EXPECT_STREQ("6", seq_rec1.to_str().c_str());

    // Exclude empty one
    Sequence seq_rec2{"1"};
    Sequence seq_sen2{""};
    seq_rec2.excl(seq_sen2);

    EXPECT_STREQ("1", seq_rec2.to_str().c_str());

    // From the middle
    Sequence seq_rec3{"1A3"};
    Sequence seq_sen3{"A"};
    seq_rec3.excl(seq_sen3);

    EXPECT_STREQ("13", seq_rec3.to_str().c_str());

    // Exclude one from another
    Sequence seq_rec4{"AAA"};
    Sequence seq_sen4{"AAA"};
    seq_rec4.excl(seq_sen4);

    EXPECT_STREQ("", seq_rec4.to_str().c_str());

    // Exclude from end
    Sequence seq_rec5{"777333"};
    Sequence seq_sen5{"333"};
    seq_rec5.excl(seq_sen5);

    EXPECT_STREQ("777", seq_rec5.to_str().c_str());

    // Cannot exclude, so keep
    Sequence seq_rec6{"7351"};
    Sequence seq_sen6{"23"};
    seq_rec6.excl(seq_sen6);

    EXPECT_STREQ("7351", seq_rec6.to_str().c_str());
}

TEST(SequenceTest, insert) {
    using namespace stud;

    // In the begining
    Sequence seq_rec1 {"123456"};
    Sequence seq_sen1 {"12"};
    seq_rec1.insert(seq_sen1, 0);

    EXPECT_STREQ("12123456", seq_rec1.to_str().c_str());

    // Insert empty
    Sequence seq_rec2{"1"};
    Sequence seq_sen2{""};
    seq_rec2.insert(seq_sen2, 0);

    EXPECT_STREQ("1", seq_rec2.to_str().c_str());

    // To the end
    Sequence seq_rec3{"1A3"};
    Sequence seq_sen3{"A"};
    seq_rec3.insert(seq_sen3, 3);

    EXPECT_STREQ("1A3A", seq_rec3.to_str().c_str());

    // To the middle
    Sequence seq_rec4{"AAA"};
    Sequence seq_sen4{"123"};
    seq_rec4.insert(seq_sen4, 2);

    EXPECT_STREQ("AA123A", seq_rec4.to_str().c_str());

}
