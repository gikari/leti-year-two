#ifndef TREE_H
#define TREE_H

#include <vector>

class B23_Tree
{
public:
    B23_Tree();
    ~B23_Tree();
    B23_Tree(const B23_Tree&);
    B23_Tree(B23_Tree&&);
    B23_Tree& operator = (const B23_Tree&);
    B23_Tree& operator = (B23_Tree&&);

    explicit B23_Tree(int*);
    explicit B23_Tree(int, B23_Tree*);
    explicit B23_Tree(const std::vector<int>&);
    explicit B23_Tree(B23_Tree*);


    void add(int);
    const B23_Tree* find(int) const;
    B23_Tree* find(int);
    //void remove(int);
    int capacity() const;
    void show() const;
    std::vector<int> get_array_representation() const;

private:
    bool node_is_empty() const;
    bool node_has_only_one_element() const;
    bool node_has_two_elements() const;
    bool node_is_leaf() const;

    void add(int*);
    void add_as_to_leaf(int*);
    void add_as_to_parent(int*);
    void split_leaf(int*);
    void push_up_value_and_connect_its_children(int*, B23_Tree*, B23_Tree*);
    void split_node_and_connect_previous_children(int*, B23_Tree*, B23_Tree*);
    void attach_children();
/*
    void replace_first_value_with_value_from_left_child();
    void replace_second_value_with_value_from_right_child();
    void reorganize_nodes();
*/
    B23_Tree* parent;
    B23_Tree* left_child;
    B23_Tree* center_child;
    B23_Tree* right_child;
    int* first_data;
    int* second_data;
};

#endif
