#include "Tree.h"
#include <vector>
#include <stdexcept>
#include <iostream>

B23_Tree::B23_Tree()
: parent{nullptr},
left_child{nullptr},
center_child{nullptr},
right_child{nullptr},
first_data{nullptr},
second_data{nullptr}
{}

B23_Tree::B23_Tree(const B23_Tree& another)
{
    first_data = another.first_data ? new int{*(another.first_data)} : nullptr;
    second_data = another.second_data ? new int{*(another.second_data)} : nullptr;

    left_child = another.left_child ? new B23_Tree{*(another.left_child)} : nullptr;
    center_child = another.center_child ? new B23_Tree{*(another.center_child)} : nullptr;
    right_child = another.right_child ? new B23_Tree{*(another.right_child)} : nullptr;

    attach_children();
}

B23_Tree::B23_Tree(B23_Tree && another)
: parent{another.parent},
left_child{another.left_child},
center_child{another.center_child},
right_child{another.right_child},
first_data{another.first_data},
second_data{another.second_data}
{
    another.parent = nullptr;
    another.left_child = nullptr;
    another.center_child = nullptr;
    another.right_child = nullptr;
    another.first_data = nullptr;
    another.second_data = nullptr;
}

B23_Tree::B23_Tree(B23_Tree* _parent)
: parent{_parent},
left_child{nullptr},
center_child{nullptr},
right_child{nullptr},
first_data{nullptr},
second_data{nullptr}
{}

B23_Tree::B23_Tree(int* value)
: parent{nullptr},
left_child{nullptr},
center_child{nullptr},
right_child{nullptr},
first_data{value},
second_data{nullptr}
{}


B23_Tree::~B23_Tree()
{
    delete left_child;
    delete center_child;
    delete right_child;
    delete first_data;
    delete second_data;
}

B23_Tree::B23_Tree(int elem, B23_Tree* _parent=nullptr)
: parent{_parent},
left_child{nullptr},
center_child{nullptr},
right_child{nullptr},
first_data{new int{elem}},
second_data{nullptr}
{}

B23_Tree::B23_Tree(const std::vector<int>& array)
: parent{nullptr},
left_child{nullptr},
center_child{nullptr},
right_child{nullptr},
first_data{nullptr},
second_data{nullptr}
{
    for (int elem : array)
    {
        add(elem);
        std::cout << "Added " << elem << std::endl;
    }
}

B23_Tree & B23_Tree::operator=(const B23_Tree& another)
{
    delete left_child;
    delete center_child;
    delete right_child;
    delete first_data;
    delete second_data;

    first_data = another.first_data ? new int{*(another.first_data)} : nullptr;
    second_data = another.second_data ? new int{*(another.second_data)} : nullptr;

    left_child = another.left_child ? new B23_Tree{*(another.left_child)} : nullptr;
    center_child = another.center_child ? new B23_Tree{*(another.center_child)} : nullptr;
    right_child = another.right_child ? new B23_Tree{*(another.right_child)} : nullptr;

    attach_children();

    return *this;
}

B23_Tree & B23_Tree::operator=(B23_Tree && another)
{
    this->parent = another.parent;
    this->left_child = another.left_child;
    this->center_child = another.center_child;
    this->right_child = another.right_child;
    this->first_data = another.first_data;
    this->second_data = another.second_data;

    another.parent = nullptr;
    another.left_child = nullptr;
    another.center_child = nullptr;
    another.right_child = nullptr;
    another.first_data = nullptr;
    another.second_data = nullptr;

    return *this;
}


void B23_Tree::add(int elem_to_add)
{
    int* new_elem = new int{elem_to_add};
    add(new_elem);
}

void B23_Tree::add(int* new_elem)
{
    if (node_is_leaf())
    {
        add_as_to_leaf(new_elem);
    }
    else
    {
        add_as_to_parent(new_elem);
    }
}

void B23_Tree::add_as_to_parent(int* new_elem)
{
    if (node_is_empty())
    {
        throw std::runtime_error("Empty node, that have child!");
    }
    else if (node_has_only_one_element())
    {
        if (*new_elem > *first_data)
        {
            center_child->add(new_elem);
        }
        else if (*new_elem <= *first_data)
        {
            left_child->add(new_elem);
        }
    }
    else if (node_has_two_elements())
    {
        if (*new_elem < *first_data)
        {
            left_child->add(new_elem);
        }
        else if (*new_elem > *first_data && *new_elem < *second_data)
        {
            center_child->add(new_elem);
        }
        else if (*new_elem > *second_data)
        {
            right_child->add(new_elem);
        }
    }
}


void B23_Tree::add_as_to_leaf(int* new_elem)
{
    if (node_is_empty())
    {
        first_data = new_elem;
    }
    else if (node_has_only_one_element())
    {
        if (*new_elem > *first_data)
        {
            second_data = new_elem;
        }
        else if (*new_elem <= *first_data)
        {
            second_data = first_data;
            first_data = new_elem;
        }
    }
    else if (node_has_two_elements())
    {
        split_leaf(new_elem);
    }
}

void B23_Tree::split_leaf(int* new_elem)
{
    int* value_to_push_up_through_tree{nullptr};
    int* value_for_left_node{nullptr};
    int* value_for_right_node{nullptr};
    if (*new_elem <= *first_data)
    {
        value_to_push_up_through_tree = first_data;
        value_for_left_node = new_elem;
        value_for_right_node = second_data;
    }
    else if (*new_elem > *first_data && *new_elem <= *second_data)
    {
        value_to_push_up_through_tree = new_elem;
        value_for_left_node = first_data;
        value_for_right_node = second_data;
    }
    else if (*new_elem > *second_data)
    {
        value_to_push_up_through_tree = second_data;
        value_for_left_node = first_data;
        value_for_right_node = new_elem;
    }

    B23_Tree* new_left_node = new B23_Tree{value_for_left_node};
    B23_Tree* new_right_node = new B23_Tree{value_for_right_node};

    if (parent)
    {
        push_up_value_and_connect_its_children(value_to_push_up_through_tree, new_left_node, new_right_node);
        new_left_node->attach_children();
        new_right_node->attach_children();

        this->first_data = nullptr;
        this->second_data = nullptr;
        this->left_child = nullptr;
        this->center_child = nullptr;
        this->right_child = nullptr;
        delete this; // commit suicide
        return;
    }
    else
    {
        this->first_data = value_to_push_up_through_tree;
        this->second_data = nullptr;
        this->left_child = new_left_node;
        this->center_child = new_right_node;
        new_left_node->attach_children();
        new_right_node->attach_children();
        this->attach_children();
    }

}

void B23_Tree::push_up_value_and_connect_its_children(int* new_elem, B23_Tree* left_elem_child, B23_Tree* right_elem_child)
{
    B23_Tree* parent_node = this->parent;
    if (!parent_node)
    {
        throw std::runtime_error("Trying to push value from node with no parent!");
    }
    if (parent_node->node_has_only_one_element())
    {
        if (*new_elem > *(parent_node->first_data))
        {
            parent_node->second_data = new_elem;
            parent_node->center_child = left_elem_child;
            parent_node->right_child = right_elem_child;
        }
        else if (*new_elem <= *(parent_node->first_data))
        {
            parent_node->second_data = parent_node->first_data;
            parent_node->first_data = new_elem;
            parent_node->right_child = parent_node->center_child;
            parent_node->center_child = right_elem_child;
            parent_node->left_child = left_elem_child;
        }
    }
    else if (parent_node->node_has_two_elements())
    {
        parent_node->split_node_and_connect_previous_children(new_elem, left_elem_child, right_elem_child);
    }
}

void B23_Tree::split_node_and_connect_previous_children(int* new_elem, B23_Tree* prev_left_elem_child, B23_Tree* prev_right_elem_child)
{
    int* value_to_push_up_through_tree{nullptr};
    int* value_for_left_node{nullptr};
    int* value_for_right_node{nullptr};

    B23_Tree* new_left_child_of_left_node{nullptr};
    B23_Tree* new_center_child_of_left_node{nullptr};
    B23_Tree* new_left_child_of_right_node{nullptr};
    B23_Tree* new_center_child_of_right_node{nullptr};

    if (*new_elem <= *first_data)
    {
        // CAME FROM LEFT
        value_to_push_up_through_tree = first_data;
        value_for_left_node = new_elem;
        value_for_right_node = second_data;

        new_left_child_of_left_node = prev_left_elem_child;
        new_center_child_of_left_node = prev_right_elem_child;
        new_left_child_of_right_node = center_child;
        new_center_child_of_right_node = right_child;
    }
    else if (*new_elem > *first_data && *new_elem <= *second_data)
    {
        // CAME FROM CENTER
        value_to_push_up_through_tree = new_elem;
        value_for_left_node = first_data;
        value_for_right_node = second_data;

        new_left_child_of_left_node = left_child;
        new_center_child_of_left_node = prev_left_elem_child;
        new_left_child_of_right_node = prev_right_elem_child;
        new_center_child_of_right_node = right_child;
    }
    else if (*new_elem > *second_data)
    {
        // CAME FROM RIGHT
        value_to_push_up_through_tree = second_data;
        value_for_left_node = first_data;
        value_for_right_node = new_elem;

        new_left_child_of_left_node = left_child;
        new_center_child_of_left_node = center_child;
        new_left_child_of_right_node = prev_left_elem_child;
        new_center_child_of_right_node = prev_right_elem_child;
    }

    B23_Tree* new_left_node = new B23_Tree{value_for_left_node};
    B23_Tree* new_right_node = new B23_Tree{value_for_right_node};

    new_left_node->left_child = new_left_child_of_left_node;
    new_left_node->center_child = new_center_child_of_left_node;
    new_right_node->left_child = new_left_child_of_right_node;
    new_right_node->center_child = new_center_child_of_right_node;

    if (parent)
    {
        push_up_value_and_connect_its_children(value_to_push_up_through_tree, new_left_node, new_right_node);
        new_left_node->attach_children();
        new_right_node->attach_children();

        this->first_data = nullptr;
        this->second_data = nullptr;
        this->left_child = nullptr;
        this->center_child = nullptr;
        this->right_child = nullptr;
        delete this; // commit suicide
        return;
    }
    else
    {
        this->first_data = value_to_push_up_through_tree;
        this->second_data = nullptr;
        this->left_child = new_left_node;
        this->center_child = new_right_node;
        new_left_node->attach_children();
        new_right_node->attach_children();
        this->attach_children();
    }
}


bool B23_Tree::node_is_empty() const
{
    return !first_data && !second_data;
}

bool B23_Tree::node_has_only_one_element() const
{
    return first_data && !second_data;
}

bool B23_Tree::node_has_two_elements() const
{
    return first_data && second_data;
}

bool B23_Tree::node_is_leaf() const
{
    return !(left_child || center_child || right_child);
}

int B23_Tree::capacity() const
{
    int cap {0};
    if (first_data) cap++;
    if (second_data) cap++;
    if (left_child) cap += left_child->capacity();
    if (center_child) cap += center_child->capacity();
    if (right_child) cap += right_child->capacity();
    return cap;
}

void B23_Tree::show() const
{
    if (second_data && first_data)
    {
        std::cout << "[ " << *first_data << ", " << *second_data << " ]" << std::endl;
    }
    else if (first_data)
    {
        std::cout << "[ " << *first_data << ", nope ]" << std::endl;
    }
    else
    {
        std::cout << "No data in node!" << std::endl;
    }
    if (left_child) left_child->show();
    if (center_child) center_child->show();
    if (right_child) right_child->show();
}

void B23_Tree::attach_children()
{
    if (this->left_child) this->left_child->parent = this;
    if (this->center_child) this->center_child->parent = this;
    if (this->right_child) this->right_child->parent = this;
}

const B23_Tree * B23_Tree::find(int value) const
{
    if ((first_data && (*first_data == value)) || (second_data && (*second_data == value)))
    {
        return const_cast<const B23_Tree*>(this);
    }

    if (first_data)
    {
        if (value < *first_data)
        {
            if (left_child)
            {
                return left_child->find(value);
            }
        }
        else if (value > *first_data)
        {
            if (second_data && value > *second_data)
            {
                if (right_child)
                    return right_child->find(value);
            }
            else if ((second_data && value < *second_data) || second_data == nullptr)
            {
                if (center_child)
                    return center_child->find(value);
            }
        }
    }
    return nullptr;
}

B23_Tree * B23_Tree::find(int value)
{
    return const_cast<B23_Tree *>(const_cast<const B23_Tree &>(*this).find(value) );
}

std::vector<int> B23_Tree::get_array_representation() const
{
    std::vector<int> array{};
    if (first_data) array.push_back(*first_data);
    if (second_data) array.push_back(*second_data);
    if (left_child)
    {
        auto subtree_elements = left_child->get_array_representation();
        array.reserve(array.size() + subtree_elements.size());
        array.insert( array.end(), subtree_elements.begin(), subtree_elements.end() );
    }
    if (center_child)
    {
        auto subtree_elements = center_child->get_array_representation();
        array.reserve(array.size() + subtree_elements.size());
        array.insert( array.end(), subtree_elements.begin(), subtree_elements.end() );
    }
    if (right_child)
    {
        auto subtree_elements = right_child->get_array_representation();
        array.reserve(array.size() + subtree_elements.size());
        array.insert( array.end(), subtree_elements.begin(), subtree_elements.end() );
    }

    return array;
}



