//
// Created by zomial on 04.03.18.
//

#include "Set.h"
#include "Tree.h"
#include <stdexcept>
#include <iostream>

Set::Set() : container{}
{
}

Set::~Set()
{}


Set::Set(const char* str) : container {} {
    for (int i = 0; str[i]; ++i)
    {
        container.add(char_to_int(str[i]));
    }
}

int Set::char_to_int(char ch) const
{
    if (ch >= '0' && ch <= '9') {
        return ch - '0';
    } else if ((ch >= 'a' && ch <= 'f') || (ch >= 'A' && ch <= 'F')) {
        switch (ch) {
        case 'a':
        case 'A':
            return 0xA;
        case 'b':
        case 'B':
            return 0xB;
        case 'c':
        case 'C':
            return 0xC;
        case 'd':
        case 'D':
            return 0xD;
        case 'e':
        case 'E':
            return 0xE;
        case 'f':
        case 'F':
            return 0xF;
        default:
            throw std::runtime_error("Unknown error!");
        }
    } else {
        throw not_represents_hex_error("Char don't represents hex digit!");
    }
}

void Set::add(char elem)
{
    add(char_to_int(elem));
}

void Set::add(int elem)
{
    if (container.capacity() < 16) {
        container.add(elem);
    } else {
        throw std::overflow_error("Too many elements in set!");
    }
}
/*
void Set::remove(char elem) {
    container.remove(char_to_int(elem));
}

void Set::remove(int elem) {
    remove(int_to_char(elem));
}
*/


std::string Set::to_str() const
{
    std::string str_rep {};
    std::vector<int> tree_array = container.get_array_representation();
    for (int elem : tree_array) {
        str_rep += int_to_char(elem);
    }
    return str_rep;
}

char Set::int_to_char(int integer) const
{
    if (integer >= 0 && integer <= 9) {
        return static_cast<char>('0' + integer);
    } else if (integer >= 0xA && integer <= 0xF) {
        return static_cast<char>('A' + integer - 0xA);
    } else {
        throw not_represents_hex_error("Integer don't represents hex!");
    }
}

void Set::show()
{
    std::cout << this->to_str() << std::endl;
}

Set::Set(const std::string& str) : container {} {
    for (auto ch : str)
    {
        container.add(char_to_int(ch));
    }
}

Set::Set(const Set& another) : container{another.container}
{}

Set::Set(Set && another)
{
    container = another.container;
}


Set& Set::operator= (const Set & another) {
    this->container = {};
    auto another_vec_repr = another.convert_to_vector();
    for (int value : another_vec_repr) {
        add(value);
    }
    return *this;
}

Set& Set::operator=(Set && another)
{
    this->container = another.container;
    return *this;
}






Set Set::operator&(const Set& another) const
{
    Set new_set {};
    auto first_set  = this->convert_to_vector();
    auto second_set  = another.convert_to_vector();
    for (int first_value : first_set) {
        for (int second_value : second_set) {
            if (first_value == second_value) {
                new_set.add(first_value);
                break;
            }
        }
    }
    return new_set;
}


Set Set::operator | (const Set& another) const
{
    Set new_set {};
    auto first_set  = this->convert_to_vector();
    auto second_set  = another.convert_to_vector();
    for (int first_value : first_set) {
        bool not_found = true;
        for (int second_value : second_set) {
            if (first_value == second_value) {
                not_found = false;
                break;
            }
        }
        if (not_found) {
            new_set.add(first_value);
        }
    }
    for (int second_value : second_set) {
        new_set.add(second_value);
    }
    return new_set;
}

Set Set::operator+(const Set& another) const
{
    Set new_set {};
    auto first_set  = this->convert_to_vector();
    auto second_set  = another.convert_to_vector();
    std::vector<int> forbidden_values {};

    for (int first_value : first_set) {
        for (int second_value : second_set) {
            if (first_value == second_value) {
                forbidden_values.push_back(first_value);
                break;
            }
        }
    }

    auto not_in_forbidden_values = [&, forbidden_values] (int checked_value)-> bool {
                for (auto value : forbidden_values) {
                    if (checked_value == value) {
                        return false;
                    }
                }
                return true;
            };

    for (int value : first_set) {
        if (not_in_forbidden_values(value))
            new_set.add(value);
    }

    for (int value : second_set) {
        if (not_in_forbidden_values(value))
            new_set.add(value);
    }


    return new_set;
}

std::vector<int> Set::convert_to_vector() const
{
    std::vector<int> vector_repr {};
    for (int element = 0x0; element <= 0xF; element++) {
        auto is_element_found = container.find(element);
        if (is_element_found) {
            vector_repr.push_back(element);
        }
    }
    return vector_repr;
}


