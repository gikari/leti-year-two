/*
 * (A or B xor C and D) and E
 * Sets as hash-tables
 * 
 */
#include <iostream>
#include <vector>
#include "Tree.h"
#include "Set.h"
// #include <string>
// #include "Set.h"

int main() {

/*
        Set test_set_a{"A2176"};
        Set test_set_b{"A214D"};
        Set test_set_c{};
        std::cout << "Started...\n";
        test_set_c = test_set_a + test_set_b;
        test_set_c.show();
        std::cout << "FINISHED! (Not yet)";
*/

    std::string set_str{};
    try {
        std::cout << "Enter set A: ";
        std::getline(std::cin, set_str);
        Set setA{ set_str };
        std::cout << "Enter set B: ";
        std::getline(std::cin, set_str);
        Set setB{ set_str };
        std::cout << "Enter set C: ";
        std::getline(std::cin, set_str);
        Set setC{ set_str };
        std::cout << "Enter set D: ";
        std::getline(std::cin, set_str);
        Set setD{ set_str };
        std::cout << "Enter set E: ";
        std::getline(std::cin, set_str);
        Set setE{ set_str };
        Set set_result{};
        set_result = ((setA | setB) + (setC & setD)) & setE;

        std::cout << "Recieved Set A: ";
        setA.show();
        std::cout << "Recieved Set A: ";
        setB.show();
        std::cout << "Recieved Set A: ";
        setC.show();
        std::cout << "Recieved Set A: ";
        setD.show();
        std::cout << "Recieved Set A: ";
        setE.show();
        std::cout << "Resulting set: " << std::endl;
        set_result.show();
    }
    catch (const not_represents_hex_error& ) {
        std::cerr << "Char don't represents hex digit!" << std::endl;
    }
    catch (const std::overflow_error&) {
        std::cerr << "Too many elements in set!" << std::endl;
    }
    catch (const std::runtime_error&) {
        std::cerr << "Unknown error!" << std::endl;
    }


    std::cout << "FINISHED!";
    return 0;
}
