#include <iostream>
#include "stud.h"

int main(int argc, char **argv) {
    std::string string1{};
    std::string string2{};
    std::cout << "Enter first sequence: ";
    std::getline(std::cin, string1);
    std::cout << "Enter second sequence: ";
    std::getline(std::cin, string2);
    stud::Sequence seq1{string1};
    stud::Sequence seq2{string2};
    stud::Sequence result{};
    std::cout << "Choose sequence operation (M - Merge; E - Excl; S - Subst) : ";
    char operation = getchar();

    if (operation == 'M' || operation == 'm') {
        result = stud::merge(seq1, seq2);
    } else if (operation == 'E' || operation == 'e') {
        result = stud::excl(seq1, seq2);
    } else if (operation == 'S' || operation == 's') {
        std::cout << "Choose insert position" << std::endl;
        size_t position;
        std::cin >> position;
        result = stud::insert(seq1, seq2, position);
    }
    std::cout << "The resulting sequence is: " << result << std::endl;

    std::cout << "Press any key to do nothing..." << std::endl;
    return 0;
}
