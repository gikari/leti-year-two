#ifndef STUD_H
#define STUD_H

#include <set>
#include <string>
#include <vector>
#include <utility>
#include <stdexcept>
#include <iterator>
#include <algorithm>

namespace stud {
    class Set;
    class Sequence;


    int char_to_int(char ch)
    {
        if (ch >= '0' && ch <= '9') {
            return ch - '0';
        } else if ((ch >= 'a' && ch <= 'f') || (ch >= 'A' && ch <= 'F')) {
            switch (ch) {
            case 'a':
            case 'A':
                return 0xA;
            case 'b':
            case 'B':
                return 0xB;
            case 'c':
            case 'C':
                return 0xC;
            case 'd':
            case 'D':
                return 0xD;
            case 'e':
            case 'E':
                return 0xE;
            case 'f':
            case 'F':
                return 0xF;
            default:
                throw std::runtime_error("Unknown error!");
            }
        } else {
            throw std::runtime_error("Char don't represents hex digit!");
        }
    }

    char int_to_char(int integer)
    {
        if (integer >= 0 && integer <= 9) {
            return static_cast<char>('0' + integer);
        } else if (integer >= 0xA && integer <= 0xF) {
            return static_cast<char>('A' + integer - 0xA);
        } else {
            throw std::runtime_error("Integer don't represents hex!");
        }
    }

    class Set : public std::set<int> {
    public:
        using std::set<int>::set;
        Set (const std::string& str)
        {
            for (auto ch : str) {
                try {
                    emplace(char_to_int(ch));
                } catch (const std::runtime_error&) {} // if not represents, skip
            }
        }

        Set operator & (const Set& rhs) const {
            Set new_one {};
            std::set_intersection(cbegin(), cend(), rhs.cbegin(), rhs.cend(), std::inserter(new_one, new_one.begin()) );
            return new_one;
        }

        Set operator | (const Set& rhs) const {
            Set new_one{};
            std::set_union(cbegin(), cend(), rhs.cbegin(), rhs.cend(), std::inserter(new_one, new_one.begin()));
            return new_one;
        }

        Set operator + (const Set& rhs) const {
            Set new_one{};
            std::set_symmetric_difference(cbegin(), cend(), rhs.cbegin(), rhs.cend(), std::inserter(new_one, new_one.begin()));
            return new_one;
        }

    private:
        // friend stud::Sequence;
    };

    std::ostream& operator << (std::ostream &os, const stud::Set& set) {
        std::string str{};
        for (auto elem : set)
            str += int_to_char(elem);
        return os << str;
    }

    class Sequence {
    public:
        Sequence() : keys{}, seq{} {};
        ~Sequence() = default;
        Sequence(const Sequence&) = default;
        Sequence(Sequence&&) = default;
        Sequence& operator = (const Sequence&) = default;
        Sequence& operator = (Sequence&&) = default;

        Sequence (const std::string& str) : keys {}, seq {}
        {
            for (auto ch : str) {
                try {
                    auto emplace_result = keys.emplace(char_to_int(ch));
                    seq.emplace_back(emplace_result.first);
                } catch (const std::runtime_error& ) {} // if not represents - ok
            }
        }

        Sequence (const std::vector<int>& vec) : keys {}, seq {} {
            for (auto elem : vec) {
                auto emplace_result = keys.emplace(elem);
                seq.emplace_back(emplace_result.first);
            }
        }

    private:
        stud::Set keys;
        std::vector <stud::Set::iterator> seq;
        friend std::ostream& operator << (std::ostream &os, const stud::Sequence& sequence);
        friend Sequence merge(const Sequence& lhs, const Sequence& rhs);
        friend Sequence excl(const Sequence& lhs, const Sequence& rhs);
        friend Sequence insert(const Sequence& lhs, const Sequence& rhs, size_t position);
    };

    std::ostream& operator << (std::ostream &os, const stud::Sequence& sequence) {
        std::string str{};
        for (auto elem : sequence.seq)
            str += int_to_char(*elem);
        return os << str;
    }

    Sequence merge(const Sequence& lhs, const Sequence& rhs) {
        Sequence new_seq{};
        std::merge(lhs.keys.cbegin(), lhs.keys.cend(), rhs.keys.cbegin(), rhs.keys.cend(), std::inserter(new_seq.keys, new_seq.keys.begin()) );

        auto sort_lambda = [] (auto first, auto second) { return *first < *second;};
        std::merge(lhs.seq.cbegin(), lhs.seq.cend(), rhs.seq.cbegin(), rhs.seq.cend(), std::inserter(new_seq.seq, new_seq.seq.begin()), sort_lambda);
        std::sort(new_seq.seq.begin(), new_seq.seq.end(), sort_lambda);
        return new_seq;
    }

    Sequence excl(const Sequence& lhs, const Sequence& rhs) {
        Sequence new_seq{};

        auto eq_lambda = [] (auto first, auto second) { return *first == *second;};
        auto pos_to_excl = std::search(lhs.seq.begin(), lhs.seq.end(), rhs.seq.begin(), rhs.seq.end(), eq_lambda);

        if (pos_to_excl != lhs.seq.end()) {
            // Change Sequence vector
            auto end_iter_of_first_seq = pos_to_excl;
            std::vector<stud::Set::iterator> first_part_of_new_seq {lhs.seq.begin(), pos_to_excl};
            std::vector<stud::Set::iterator> second_part_of_new_seq {pos_to_excl + rhs.seq.size(), lhs.seq.end()};
            first_part_of_new_seq.insert(first_part_of_new_seq.end(), second_part_of_new_seq.begin(), second_part_of_new_seq.end());

            std::vector<int> new_seq_vec{};
            for (auto elem : first_part_of_new_seq) {
                new_seq_vec.emplace_back(*elem);
            }

            new_seq = Sequence{new_seq_vec};

        } else {
            std::cerr << "Excluding sequence not found!\n";
        }

        return new_seq;
    }

    Sequence insert(const Sequence& lhs, const Sequence& rhs, size_t position) {
        Sequence new_seq{};
        std::merge(lhs.keys.cbegin(), lhs.keys.cend(), rhs.keys.cbegin(), rhs.keys.cend(), std::inserter(new_seq.keys, new_seq.keys.begin()) );
        new_seq.seq = lhs.seq;

        try {
            new_seq.seq.insert(new_seq.seq.begin() + position, rhs.seq.begin(), rhs.seq.end());
        } catch (...) {
            std::cerr << "Unknown error has happen!\n";
            return Sequence{};
        }

        return new_seq;

    }


}

#endif
