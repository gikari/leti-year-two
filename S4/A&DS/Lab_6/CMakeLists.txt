cmake_minimum_required(VERSION 2.6)
project(lab_6)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

add_executable(lab_6 main.cpp)

install(TARGETS lab_6 RUNTIME DESTINATION bin)
