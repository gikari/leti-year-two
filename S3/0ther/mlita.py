#!/usr/bin/env python3

class WrongGrammar(Exception):
    pass

symbols_set = "abcABC "

phrase = ""
last_char = '/\\'
index = -1

def next_char(char):
    global index
    global last_char
    last_char = char
    #print(char, end='')
    try:
        index += 1
        new_char = phrase[index]
        return new_char
    except IndexError:
        #print("\nINDEX ERROR")
        index = len(phrase)
        return '/\\'

def in_set(char, _set):
    if _set.find(char) == -1:
        return False
    else:
        return True;


def quotes_lang(char):
    global last_char
    if char == '<':
        char = next_char(char)
        char = prob_w_quotes(char)
        if (char == '>') != (last_char == '>'):
            char = next_char(char)
        else:
            raise WrongGrammar('No >')
    elif in_set(char, symbols_set):
        char = symbols(char)
        char = quotes_lang(char)
    else:
        pass
    return char

def prob_w_quotes(char):
    if in_set(char, symbols_set):
        char = symbols(char)
        char = prob_w_quotes(char)
    elif char == '<':
        char = next_char(char)
        char = symbols(char)
        char = prob_right_q(char)
    else:
        pass
    return char

def prob_right_q(char):
    if char == '>':
        char = next_char(char)
        char = symbols(char)
        char = prob_w_quotes(char)
    elif in_set(char, symbols_set):
        char = symbols(char)
        char = prob_right_q(char)
    elif char == '<':
        char = next_char(char)
        char = symbols(char)
        char = prob_right_q(char)
    else:
        pass
    return char


def symbols(char):
    if char == 'a':
        char = next_char(char)
        char = symbols(char)
    elif char == 'b':
        char = next_char(char)
        char = symbols(char)
    elif char == 'c':
        char = next_char(char)
        char = symbols(char)
    if char == 'A':
        char = next_char(char)
        char = symbols(char)
    elif char == 'B':
        char = next_char(char)
        char = symbols(char)
    elif char == 'C':
        char = next_char(char)
        char = symbols(char)
    elif char == ' ':
        char = next_char(char)
        char = symbols(char)
    else:
        pass
    return char


print("Правило русского языка: Если внутри слов, заключенных в кавычки, встречаются другие слова, в свою очередь заключенные в кавычки, то закрывающие кавычки ставятся только один раз, например, ЗАО <Издательский дом <Комсомольская правда>, OOO <Компания <Металлинвест>,", "\nПример: ЗАО <Заза <Озаза>", "\n <abc<abc>abc>")
while True:
    phrase = input("Введите строку (a, b, c, пробел, <, > разрешены) : ")
    last_char = '/\\'
    index = -1
    try:
        index = 0
        quotes_lang(phrase[0])
        if index != len(phrase):
            raise WrongGrammar("Wrong Grammar")
        print("Грамматика подходит")
    except WrongGrammar:
        #print("\nIndex:", index, "\nLen:",  len(phrase))
        print("Грамматика не подходит")
