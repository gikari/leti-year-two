#include <gtest/gtest.h>
#include "../sets.h"

TEST(Unite, Equals) {
    // Arrange
    char A[] = "012D";
    char B[] = "012D";
    HexSet setA{"012D"};
    HexSet setB{"012D"};

    // Act
    char* AB = ::SetsOperations::unite(A, B);
    HexSet uAB {setA|setB};

    // Assert
    EXPECT_STREQ("012D", AB);
    EXPECT_TRUE(uAB == "012D");
}

TEST(Unite, Different) {
    char C[] = "0342";
    char D[] = "9CEF";
    HexSet setA{"0342"};
    HexSet setB{"9CEF"};

    char* CD = ::SetsOperations::unite(C, D);
    HexSet uAB {setA|setB};

    EXPECT_STREQ("03429CEF", CD);
    EXPECT_TRUE(uAB == "02349CEF");
}

TEST(Unite, WithEmpty) {
    char E[] = "";
    char F[] = "CA";
    HexSet setA{""};
    HexSet setB{"CA"};

    char* EF = ::SetsOperations::unite(E, F);
    HexSet uAB{setA|setB};

    EXPECT_STREQ("CA", EF);
    EXPECT_TRUE(uAB == "AC");
}

TEST(Unite, TwoCommon) {
    char G[] = "125";
    char H[] = "135";
    HexSet setA{"125"};
    HexSet setB{"135"};

    char* GH = ::SetsOperations::unite(G, H);
    HexSet uAB{setA|setB};

    EXPECT_STREQ("2135", GH);
    EXPECT_TRUE(uAB == "1235");
}

TEST(Unite, OneCommon) {
    char I[] = "975";
    char J[] = "E69";
    HexSet setA{"975"};
    HexSet setB{"E69"};

    char* IJ = ::SetsOperations::unite(I, J);
    HexSet uAB{setA|setB};

    EXPECT_STREQ("75E69", IJ);
    EXPECT_TRUE(uAB == "5679E");
}

TEST(Unite, BothEmpty) {
    char I[] = "";
    char J[] = "";
    HexSet setA{""};
    HexSet setB{""};

    char* IJ = ::SetsOperations::unite(I, J);
    HexSet uAB{setA|setB};

    EXPECT_STREQ("", IJ);
    EXPECT_TRUE(uAB == "");
}

TEST(Cross, Equals) {
    char A[] = "2AB";
    char B[] = "2AB";
    HexSet setA{"2AB"};
    HexSet setB{"2AB"};

    char* AB = ::SetsOperations::cross(A, B);
    HexSet cAB{setA & setB};

    EXPECT_STREQ("2AB", AB);
    EXPECT_TRUE(cAB == "2AB");
}

TEST(Cross, Different) {
    char A[] = "2AB";
    char B[] = "7CF";
    HexSet setA{"2AB"};
    HexSet setB{"7CF"};

    char* AB = ::SetsOperations::cross(A, B);
    HexSet cAB{setA & setB};

    EXPECT_STREQ("", AB);
    EXPECT_TRUE(cAB == "");
}

TEST(Cross, WithEmpty) {
    char A[] = "AB2";
    char B[] = "";
    HexSet setA{"AB2"};
    HexSet setB{""};

    char* AB = ::SetsOperations::cross(A, B);
    HexSet cAB{setA & setB};

    EXPECT_STREQ("", AB);
    EXPECT_TRUE(cAB == "");
}

TEST(Cross, TwoCommon) {
    char A[] = "12C";
    char B[] = "14C";
    HexSet setA{"12C"};
    HexSet setB{"14C"};

    char* AB = ::SetsOperations::cross(A, B);
    HexSet cAB{setA & setB};

    EXPECT_STREQ("1C", AB);
    EXPECT_TRUE(cAB == "1C");
}

TEST(Cross, OneCommon) {
    char A[] = "AB2";
    char B[] = "F02";
    HexSet setA{"AB2"};
    HexSet setB{"F02"};

    char* AB = ::SetsOperations::cross(A, B);
    HexSet cAB{setA & setB};

    EXPECT_STREQ("2", AB);
    EXPECT_TRUE(cAB == "2");
}

TEST(Cross, BothEmpty) {
    char A[] = "";
    char B[] = "";
    HexSet setA{""};
    HexSet setB{""};

    char* AB = ::SetsOperations::cross(A, B);
    HexSet cAB{setA & setB};

    EXPECT_STREQ("", AB);
    EXPECT_TRUE(cAB == "");
}

TEST(HexSetClass, CorrectTranslation) {
    HexSet someSet{"123BD"};

    const char* str_representation = someSet.to_string().c_str();

    EXPECT_STREQ(str_representation, "123BD");
}
