#include "conio.h"
#include <unistd.h>


char* getcolor(int i);

int main() {
    const int PERIOD_MS = 1200000;
    const int TOTAL_COLORS = 16;

    window(25, 8, 55, 18);

    usleep(PERIOD_MS);
    for (int i = 0; i < TOTAL_COLORS; ++i) {
        textbackground(i);
        for (int j = 0; j < TOTAL_COLORS; ++j) {
            textcolor(j);
            cprintf("BG %s FG %s\n\n\n", getcolor(i), getcolor(j));
            usleep(PERIOD_MS);
        }
    }
    cprintf("THE END!");

    return 0;
}

char* getcolor(int i){
    switch (i) {
        case 0:
            return "4EPHblu";
            break;
        case 1:
            return "KPACHblu";
            break;
        case 2:
            return "3EJlEHblu";
            break;
        case 3:
            return "KOPu4HEBblu";
            break;
        case 4:
            return "CuHuu";
            break;
        case 5:
            return "9PuOJlETOBblu";
            break;
        case 6:
            return "ToJlybou";
            break;
        case 7:
            return "CBETJlO-CEPblu";
            break;
        case 8:
            return "TEMHO-CEPblu";
            break;
        case 9:
            return "CBETJlO-KPACHblu";
            break;
        case 10:
            return "CBETJlO-3EJlEHblu";
            break;
        case 11:
            return ">KEJlTblu";
            break;
        case 12:
            return "ToJlybou";
            break;
        case 13:
            return "PO3OBblu";
            break;
        case 14:
            return "CBETJlO-ToJlybou";
            break;
        case 15:
            return "HEbECHblu";
            break;
        default:
            return "UNDEFINED";
    }
};
