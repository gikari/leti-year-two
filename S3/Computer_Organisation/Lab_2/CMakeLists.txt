cmake_minimum_required(VERSION 2.6)
project(lab_2)

find_package(Curses REQUIRED)
include_directories(${CURSES_INCLUDE_DIR})

add_executable(lab_2 main.cpp)
target_link_libraries(lab_2 ${CURSES_LIBRARIES})

install(TARGETS lab_2 RUNTIME DESTINATION bin)
