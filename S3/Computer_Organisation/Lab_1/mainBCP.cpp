#include <iostream>
#include <string>
#include <climits>

std::string long_to_bits(long);
std::string double_to_bits(double);

long cyclic_shift(long, bool right_dir, int amount);

union PseudoDouble
{
    double trueDouble;
    long long fakeDouble;
};

int main(int argc, char **argv) {
    using namespace std;
    char choice = 0;
    long l_number = 0;
    double d_number = 0;
    cout << "L - show bits representation of long" << endl
         << "D - show bits representation of double" << endl;
    if (choice == 'L') {
        cout << "Input long: ";
        cin >> l_number;
        cout << "Long bits representation: " << long_to_bits(l_number) << endl;
    } else if (choice == 'D') {
        cout << "Input double: ";
        cin >> d_number;
        cout << "Long bits representation: " << double_to_bits(d_number) << endl;
    }
    cout << "Cyclic shift to right or to the left? (r\\l)" << endl;
    cin >> choice;
    int shift_amount = 0;
    cout << "Number of bits to shift?";
    cin >> shift_amount;

    long shift_result = cyclic_shift(l_number, choice == 'r' ? true : false, shift_amount);


    return 0;
}

std::string long_to_bits(long number) {
    std::string byte_str{};
    for (int i = 8*sizeof(long) - 1; i >= 0; --i) {
        byte_str += (number >> i) & 1 ? '1' : '0';
        if (i%4 == 0) byte_str += ' ';
    }
    return byte_str;
}

std::string double_to_bits(double num) {
    std::string byte_str{};
    PseudoDouble pd;
    pd.trueDouble = num;
    long long number = pd.fakeDouble;
    for (int i = CHAR_BIT*sizeof(double) - 1; i >= 0; --i) {
        byte_str += (number >> i) & 1 ? '1' : '0';
        if (i == CHAR_BIT*sizeof(long) - 1 || i == CHAR_BIT*sizeof(long) - 1 - 11) byte_str += "  ";
        if (i%4 == 0) byte_str += ' ';
    }
    return byte_str;
}

long cyclic_shift(unsigned long number, bool right_dir, int amount)
{
    if (right_dir)
        return (number >> amount) | (number << (CHAR_BIT * sizeof(long) - 1));
    else
        return (number << amount) | (number >> (CHAR_BIT * sizeof(long) - 1));
}
