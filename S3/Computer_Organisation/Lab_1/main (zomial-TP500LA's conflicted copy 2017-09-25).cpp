#include <iostream>
#include <string>

std::string long_to_bits(long);
std::string double_to_bits(double);



union PseudoDouble
{
    double trueDouble;
    long fakeDouble;
};

int main(int argc, char **argv) {
    using namespace std;

    cout << double_to_bits(15.375) << "\n";
    return 0;
}

std::string long_to_bits(long number) {
    std::string byte_str{};
    for (int i = 8*sizeof(long) - 1; i >= 0; --i) {
        byte_str += (number >> i) & 1 ? '1' : '0';
        if (i%4 == 0) byte_str += ' ';
    }
    return byte_str;
}

std::string double_to_bits(double num) {
    std::string byte_str{};
    PseudoDouble pd;
    pd.trueDouble = num;
    long number = pd.fakeDouble;
    for (int i = 8*sizeof(double) - 1; i >= 0; --i) {
        byte_str += (number >> i) & 1 ? '1' : '0';
        if (i == 8*sizeof(long) - 1 || i == 8*sizeof(long) - 1 - 11) byte_str += "  ";
        if (i%4 == 0) byte_str += ' ';
    }
    return byte_str;
}


