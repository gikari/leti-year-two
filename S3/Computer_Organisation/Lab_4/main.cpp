#include "conio.h"
#include <unistd.h>

enum {ESC = 27, UP = 53, DOWN = 52};

void print_ch_n_times(char, int);

int main() {

    window(25, 8, 55, 18);
    textbackground(5);
    textcolor(1);

    int  position = 0;
    cprintf("*");
    while (true) {
        if (!position) position = 11;

        int key = getch();
        //int key = 3;

        asm (
            /"subl $256, %%esp;"
            //"movl $3, %%eax;"
            //"movl $0, %%ebx;"
            //"movl %%esp, %%ecx;"
            //"movl $256, %%edx;"
            //"int $0x80;"
            ""
            ""
            :
            :
            //: "esp", "eax", "ebx", "ecx", "edx"
        );

        //cprintf("^%d^\n", key);
        if (key == ESC)
            break;
        else if (key == UP) {
            //cprintf("BAKA");
            clrscr();
            print_ch_n_times('\n', (--position%11));
            cprintf("*");
        } else if (key == DOWN) {
            clrscr();
            print_ch_n_times('\n', (++position%11));
            cprintf("*");
        }
    }

    return 0;
}

void print_ch_n_times(char ch, int n)
{
    for (int i = 0; i < n; ++i)
        cprintf("%c", ch);
}
