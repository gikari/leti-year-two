#include <graphics.h>
#include <unistd.h>
#include <math.h>

const int   left     = 10;
const int   top      = 10;
const int   right    = 1350;
const int   bottom   = 750;

const int   margin   = 10;
const int   width    = right - left - 2*margin;
const int   height   = bottom - top - 2*margin;
const int   w_center = width/2;
const int   h_center = height/2;

const float begin_x  = 3*M_PI/2;
const float end_x    = 8*M_PI;

const float step     = 0.001;
const float pers     = 0.003;

int get_scaled_argument_y(float not_scaled);
int get_scaled_argument_x(float not_scaled);
int near_eq(float, float);

int main() {
    float y_max = -10000;

    int gd = DETECT, gm;
    initgraph(&gd, &gm, NULL);
    setcolor(BLUE);

    rectangle(left + margin, top + margin, right - margin, bottom - margin);
    line(left+margin, top + h_center, right - margin, top + h_center);
    //line(left + width/2, top + margin, left + width/2, bottom - margin);

    setcolor(WHITE);
    for (float x = begin_x; x < end_x; x += step) {
        float y = pow(sin(x), 3) + pow(cos(x), 2);

        if (y > y_max)
            y_max = y;
        //float y = cos(x);
        //float y = sin(x);
        int scaled_x = get_scaled_argument_x(x), scaled_y = get_scaled_argument_y(y);
        putpixel(scaled_x, scaled_y, YELLOW);

        if (near_eq(x, begin_x)) {
            line(scaled_x, h_center, scaled_x, h_center+2*margin);
            moveto(scaled_x + 5, height/2 + margin);
            outtext("3 PI / 2");
        } else if (near_eq(x, 2*M_PI)) {
            line(scaled_x, h_center, scaled_x, h_center+2*margin);
            moveto(scaled_x + 5, height/2 + margin);
            outtext("2 PI");
        } else if (near_eq(x, 5*M_PI/2)) {
            line(scaled_x, h_center, scaled_x, h_center+2*margin);
            moveto(scaled_x + 5, height/2 + margin);
            outtext("5 PI / 2");
        } else if (near_eq(x, 3*M_PI)) {
            line(scaled_x, h_center, scaled_x, h_center+2*margin);
            moveto(scaled_x + 5, height/2 + margin);
            outtext("3 PI");
        } else if (near_eq(x, 7*M_PI/2)) {
            line(scaled_x, h_center, scaled_x, h_center+2*margin);
            moveto(scaled_x + 5, height/2 + margin);
            outtext("7 PI / 2");
        } else if (near_eq(x, 4*M_PI)) {
            line(scaled_x, h_center, scaled_x, h_center+2*margin);
            moveto(scaled_x + 5, height/2 + margin);
            outtext("4 PI");
        } else if (near_eq(x, 9*M_PI/2)) {
            line(scaled_x, h_center, scaled_x, h_center+2*margin);
            moveto(scaled_x + 5, height/2 + margin);
            outtext("9 PI / 2");
        } else if (near_eq(x, 5*M_PI)) {
            line(scaled_x, h_center, scaled_x, h_center+2*margin);
            moveto(scaled_x + 5, height/2 + margin);
            outtext("5 PI");
        } else if (near_eq(x, 11*M_PI/2)) {
            line(scaled_x, h_center, scaled_x, h_center+2*margin);
            moveto(scaled_x + 5, height/2 + margin);
            outtext("11 PI / 2");
        } else if (near_eq(x, 6*M_PI)) {
            line(scaled_x, h_center, scaled_x, h_center+2*margin);
            moveto(scaled_x + 5, height/2 + margin);
            outtext("6 PI");
        } else if (near_eq(x, 13*M_PI/2)) {
            line(scaled_x, h_center, scaled_x, h_center+2*margin);
            moveto(scaled_x + 5, height/2 + margin);
            outtext("13 PI / 2");
        } else if (near_eq(x, 7*M_PI)) {
            line(scaled_x, h_center, scaled_x, h_center+2*margin);
            moveto(scaled_x + 5, height/2 + margin);
            outtext("7 PI");
        } else if (near_eq(x, 15*M_PI/2)) {
            line(scaled_x, h_center, scaled_x, h_center+2*margin);
            moveto(scaled_x + 5, height/2 + margin);
            outtext("15 PI / 2");
        } else if (near_eq(x, end_x)) {
            line(scaled_x, h_center, scaled_x, h_center+2*margin);
            moveto(scaled_x + 5, height/2 + margin);
            outtext("8 PI");
        }
    }

    settextstyle(1, 0, 20);
    moveto(900, 200);
    outtext("Max is: ");
    char max_str[10];
    sprintf(max_str, "%f", y_max);
    outtext(max_str);

    getch();

    //sleep(3);

    closegraph();
    return 0;
}

int get_scaled_argument_y(float not_scaled)
{
    int scaled = (top + margin) + h_center + not_scaled*(float(height)/(2));
    return scaled;
}

int get_scaled_argument_x(float not_scaled)
{
    int scaled = (left + margin) + (not_scaled - begin_x)*(float(width)/(end_x - begin_x));
    return scaled;
}

int near_eq(float a, float b)
{
    return (fabs(a-b) < pers) ? 1 : 0;
}

